package info.androidhive.navigationdrawer.fragment;

import android.app.ExpandableListActivity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import java.util.ArrayList;

import info.androidhive.navigationdrawer.MyExpandableAdapter;
import info.androidhive.navigationdrawer.R;


public class ApproveFragment extends Fragment/* implements ExpandableListActivity */{

    public ApproveFragment() {
    }

    private ArrayList<String> parentItems = new ArrayList<String>();
    private ArrayList<Object> childItems = new ArrayList<Object>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
/*
        ExpandableListView expandableList = getExpandableListView(); // you can use (ExpandableListView) findViewById(R.id.list)
        expandableList.setDividerHeight(2);
        expandableList.setGroupIndicator(null);
        expandableList.setClickable(true);*/

        setGroupParents();
        setChildData();

        MyExpandableAdapter adapter = new MyExpandableAdapter(parentItems, childItems);

     /*   adapter.setInflater((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE), getActivity());
        expandableList.setAdapter(adapter);
        expandableList.setOnChildClickListener(this);*/

        return inflater.inflate(R.layout.fragment_photos, container, false);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }
    public void setGroupParents() {
        parentItems.add("Android");
        parentItems.add("Core Java");
        parentItems.add("Desktop Java");
        parentItems.add("Enterprise Java");
    }

    public void setChildData() {

        // Android
        ArrayList<String> child = new ArrayList<String>();
        child.add("Core");

        childItems.add(child);


        // Core Java
        child = new ArrayList<String>();
        child.add("Apache");

        childItems.add(child);

        // Desktop Java
        child = new ArrayList<String>();
        child.add("Accessibility");

        childItems.add(child);


        // Enterprise Java
        child = new ArrayList<String>();
        child.add("EJB3");

        childItems.add(child);
    }


}
