package info.androidhive.navigationdrawer.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import info.androidhive.navigationdrawer.Po_Model;
import info.androidhive.navigationdrawer.R;
import info.androidhive.navigationdrawer.app.AppConfig;
import info.androidhive.navigationdrawer.app.AppController;
import info.androidhive.navigationdrawer.helper.SQLiteHandler;
import info.androidhive.navigationdrawer.helper.SessionManager;
import info.androidhive.navigationdrawer.support.HideKeyBoard;

import static info.androidhive.navigationdrawer.app.AppController.TAG;

/**
 * Created by Syss4 on 9/21/2017.
 */

public class So_Approval_Fragment extends Fragment {

    EditText eMail, passWord;
    TextView acc, com, ids;
    Button login;
    TextView not_reg, imei;
    SessionManager sessionManager;
    private SQLiteHandler db;
    String uName, uPwd;
    private HideKeyBoard hideKeyBoard;
    SQLiteHandler dbase;
    TextView org_id, user_id;
    List<Po_Model> poModelList;
    private ListView listView;
    Button logs;
    private ProgressDialog pDialog;
    private SessionManager session;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.apro, null);

        listView = (ListView)rootView.findViewById(R.id.list_view);
        org_id = (TextView)rootView.findViewById(R.id.orgIds);
        user_id = (TextView)rootView.findViewById(R.id.userIds);
        //  logs = (Button)rootView.findViewById(R.id.log);

        poModelList = new ArrayList<>();

        dbase = new SQLiteHandler(getActivity());

        HashMap<String, String> user = dbase.getUserDetails();
        String uId = user.get("uid");
        String orgId = user.get("ss_defaultorg_id");

        org_id.setText(orgId);
        user_id.setText(uId);


        pDialog = new ProgressDialog(getActivity());
        pDialog.setCancelable(false);


        String uid = user_id.getText().toString();
        String org_ids = org_id.getText().toString();

        checkLogin(uid, org_ids);

        return rootView;
    }


    private void checkLogin(final String uid, final String orgid) {
        // Tag used to cancel the request
        String tag_string_req = "req_login";

        pDialog.setMessage("Logging in ...");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST, AppConfig.SO_APPROVAL, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Approval Response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                    Log.w("Aruna", "Viswa");
                    // Check for error node in json
                    if (!error) {
                        // user successfully logged in
                        // Create login session

                        Log.w("Viswa", "kumar");
                        // Now store the user in SQLite

                        JSONArray polist = jObj.getJSONArray("polist");
                        for (int i = 0; i < polist.length(); i++) {

                            Log.w("sai", "pavan");

                            JSONObject c = polist.getJSONObject(i);

                            String po_number = c.getString("po_number");
                            String po_header_id = c.getString("po_header_id");
                            String po_date = c.getString("po_date");
                            String emp_id = c.getString("emp_id");
                            String buyername = c.getString("buyername");
                            String username = c.getString("username");
                            String supplier_id = c.getString("supplier_id");
                            String supplier_name = c.getString("supplier_name");
                            String po_approval_status = c.getString("po_approval_status");
                            String remarks = c.getString("remarks");
                            String organization_id = c.getString("organization_id");
                            String supervisor_user_id = c.getString("supervisor_user_id");
                            String user_id = c.getString("user_id");

                            Log.w("poNumber", po_number);
                            Log.e("podate", po_date);
                            Log.d("empId", emp_id);
                            Log.w("uId" ,user_id);
                            Po_Model poModel = new Po_Model(po_number, po_header_id, po_date, emp_id, buyername, username, supplier_id, supplier_name, po_approval_status, remarks, organization_id, supervisor_user_id, user_id);
                            poModelList.add(poModel);

//finish();
                        }

                        info.androidhive.navigationdrawer.So_List_Adapter adapter = new info.androidhive.navigationdrawer.So_List_Adapter(poModelList, getActivity());

                        listView.setAdapter(adapter);

                    } else {
                        // Error in login. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getActivity(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Login Error: " + error.getMessage());
                Toast.makeText(getActivity(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
               /* String orgid = "17";
                String uid = "51";*/

                String orgid = org_id.getText().toString();
                String uid = user_id.getText().toString();

                params.put("ss_defaultorg_id", orgid);
                params.put("user_id", uid);
                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }


}
