package info.androidhive.navigationdrawer.datas.models.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Comp11 on 1/9/2018.
 */

public class LeaveFormResponse {

    @SerializedName("leave_type")
    @Expose
    private List<LeaveType> leaveType = null;
    @SerializedName("employee_details")
    @Expose
    private EmployeeDetails employeeDetails;
    @SerializedName("type_leave")
    @Expose
    private List<TypeLeave> typeLeave = null;
    @SerializedName("message")
    @Expose
    private String message;

    public List<LeaveType> getLeaveType() {
        return leaveType;
    }

    public void setLeaveType(List<LeaveType> leaveType) {
        this.leaveType = leaveType;
    }

    public EmployeeDetails getEmployeeDetails() {
        return employeeDetails;
    }

    public void setEmployeeDetails(EmployeeDetails employeeDetails) {
        this.employeeDetails = employeeDetails;
    }

    public List<TypeLeave> getTypeLeave() {
        return typeLeave;
    }

    public void setTypeLeave(List<TypeLeave> typeLeave) {
        this.typeLeave = typeLeave;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
