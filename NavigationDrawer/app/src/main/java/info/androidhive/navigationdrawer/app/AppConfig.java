package info.androidhive.navigationdrawer.app;

public class AppConfig {


    public static String BASE_URL = "http://192.168.1.99/symtecapp/symtecdevdeployc/public/mobileapputlity/";

    //public static String BASE_URL = "http://ifive.sytes.net/symtecapp/symtecdevdeploy/public/mobileapputlity/";

    public static String URL_LOGIN = BASE_URL + "validateuser";
    public static String PO_APPROVAL = BASE_URL + "poapproval";
    public static String PO_APPCONFIRM = BASE_URL + "approveaction";
    public static String SO_APPROVAL = BASE_URL + "solistapproval";
    public static String COMPANY = BASE_URL + "companydetails";
    public static String LEAVE_APPROVAL = BASE_URL + "companydetails";

    public static String NOTIFICATION = BASE_URL + "notificationlist";
    public static String NOTIFICATION_FOR_ME = BASE_URL + "notificationlisttome";
    public static String NOTIFICATION_APPROVAL = BASE_URL + "notificationapproval";


    public static String URL_CUSTOMER = BASE_URL + "customerdetails";

}
