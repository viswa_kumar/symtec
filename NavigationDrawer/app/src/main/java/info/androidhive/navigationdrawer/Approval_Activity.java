package info.androidhive.navigationdrawer;

import android.app.Activity;
import android.app.ProgressDialog;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import info.androidhive.navigationdrawer.app.AppConfig;
import info.androidhive.navigationdrawer.app.AppController;
import info.androidhive.navigationdrawer.helper.SQLiteHandler;
import info.androidhive.navigationdrawer.helper.SessionManager;
import info.androidhive.navigationdrawer.support.HideKeyBoard;

import static info.androidhive.navigationdrawer.app.AppController.TAG;

/**
 * Created by Syss4 on 9/18/2017.
 */

public class Approval_Activity extends Activity {

    EditText eMail, passWord;
    TextView acc, com, ids;
    Button login;
    TextView not_reg, imei;
    SessionManager sessionManager;
    private SQLiteHandler db;
    String uName, uPwd;
    private HideKeyBoard hideKeyBoard;
    SQLiteHandler dbase;
    TextView org_id, user_id;
    List<Po_Model> poModelList;
    private ListView listView;
    Button logs;
    private ProgressDialog pDialog;
    private SessionManager session;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.apro);

        listView = (ListView) findViewById(R.id.list_view);
        org_id = (TextView)findViewById(R.id.orgIds);
        user_id = (TextView)findViewById(R.id.userIds);
       // logs = (Button)findViewById(R.id.log);

        poModelList = new ArrayList<>();

        dbase = new SQLiteHandler(getApplicationContext());

        HashMap<String, String> user = dbase.getUserDetails();
        String uId = user.get("uid");
        String orgId = user.get("ss_defaultorg_id");

        org_id.setText(orgId);
        user_id.setText(uId);


        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);


        String uid = user_id.getText().toString();
        String org_ids = org_id.getText().toString();

        checkLogin(uid, org_ids);


       /* logs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (org_id.getText().toString().length() == 0) {
                    org_id.setError("Email id is required!");
                }


                if (user_id.getText().toString().length() == 0) {
                    user_id.setError("Wrong Password!");
                } else {
                    String user_ids = user_id.getText().toString();
                    String org_ids = org_id.getText().toString();



                    Log.d(user_ids, "This is my message");
                    // Check for empty data in the form
                    if (!user_ids.isEmpty() && !org_ids.isEmpty()) {
                        // login user
                        checkLogin(user_ids, org_ids);


                    } else {
                        // Prompt user to enter credentials
                        Toast.makeText(Approval_Activity.this, "Please enter correct email & password", Toast.LENGTH_SHORT).show();

                    }
                }
            }
        });*/
    }


    private void checkLogin(final String uid, final String orgid) {
        // Tag used to cancel the request
        String tag_string_req = "req_login";

        pDialog.setMessage("Logging in ...");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST, AppConfig.PO_APPROVAL, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Approval Response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                    Log.w("Aruna", "Viswa");
                    // Check for error node in json
                    if (!error) {
                        // user successfully logged in
                        // Create login session

                        Log.w("Viswa", "kumar");
                        // Now store the user in SQLite

                        JSONArray polist = jObj.getJSONArray("polist");
                        for (int i = 0; i < polist.length(); i++) {

                            Log.w("sai", "pavan");

                            JSONObject c = polist.getJSONObject(i);

                            String po_number = c.getString("po_number");
                            String po_header_id = c.getString("po_header_id");
                            String po_date = c.getString("po_date");
                            String emp_id = c.getString("emp_id");
                            String buyername = c.getString("buyername");
                            String username = c.getString("username");
                            String supplier_id = c.getString("supplier_id");
                            String supplier_name = c.getString("supplier_name");
                            String po_approval_status = c.getString("po_approval_status");
                            String remarks = c.getString("remarks");
                            String organization_id = c.getString("organization_id");
                            String supervisor_user_id = c.getString("supervisor_user_id");
                            String user_id = c.getString("user_id");

                            Log.w("poNumber", po_number);
                            Log.e("podate", po_date);
                            Log.d("empId", emp_id);
                            Po_Model poModel = new Po_Model(po_number, po_header_id, po_date, emp_id, buyername, username, supplier_id, supplier_name, po_approval_status, remarks, organization_id, supervisor_user_id, user_id);
                            poModelList.add(poModel);

//finish();
                        }

                        ListAdapter adapter = new ListAdapter(poModelList, getApplicationContext());

                        listView.setAdapter(adapter);

                    } else {
                        // Error in login. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Login Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
               /* String orgid = "17";
                String uid = "51";*/

                String orgid = org_id.getText().toString();
                String uid = user_id.getText().toString();

                params.put("ss_defaultorg_id", orgid);
                params.put("user_id", uid);
                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

   /* @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }*/

    public final boolean isInternetOn() {

        // get Connectivity Manager object to check connection
        ConnectivityManager connec =
                (ConnectivityManager) getSystemService(getBaseContext().CONNECTIVITY_SERVICE);

        // Check for network connections
        if (connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED) {

            // if connected with internet

//            Toast.makeText(this, " Connected ", Toast.LENGTH_SHORT).show();
            return true;

        } else if (
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED ||
                        connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED) {

            Toast.makeText(this, "Please Check Your Internet Connection!", Toast.LENGTH_SHORT).show();
            return false;
        }
        return false;
    }
}
