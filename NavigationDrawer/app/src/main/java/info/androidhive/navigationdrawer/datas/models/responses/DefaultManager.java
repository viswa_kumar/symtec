package info.androidhive.navigationdrawer.datas.models.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Comp11 on 1/9/2018.
 */

public class DefaultManager {

    @SerializedName("employee_id")
    @Expose
    private String employeeId;
    @SerializedName("employee_number")
    @Expose
    private String employeeNumber;
    @SerializedName("employee_count")
    @Expose
    private String employeeCount;
    @SerializedName("prefix")
    @Expose
    private String prefix;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("photo")
    @Expose
    private String photo;
    @SerializedName("role")
    @Expose
    private String role;
    @SerializedName("date_of_birth")
    @Expose
    private String dateOfBirth;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("company_id")
    @Expose
    private String companyId;
    @SerializedName("location_id")
    @Expose
    private String locationId;
    @SerializedName("department")
    @Expose
    private String department;
    @SerializedName("reporting_manager")
    @Expose
    private String reportingManager;
    @SerializedName("job_title")
    @Expose
    private String jobTitle;
    @SerializedName("position")
    @Expose
    private String position;
    @SerializedName("flat")
    @Expose
    private String flat;
    @SerializedName("employment_status")
    @Expose
    private String employmentStatus;
    @SerializedName("date_of_joining")
    @Expose
    private String dateOfJoining;
    @SerializedName("date_of_leaving")
    @Expose
    private String dateOfLeaving;
    @SerializedName("years_of_experience")
    @Expose
    private String yearsOfExperience;
    @SerializedName("esi_no")
    @Expose
    private String esiNo;
    @SerializedName("pf_no")
    @Expose
    private String pfNo;
    @SerializedName("uan_no")
    @Expose
    private String uanNo;
    @SerializedName("work_telephone_number")
    @Expose
    private String workTelephoneNumber;
    @SerializedName("extension")
    @Expose
    private String extension;
    @SerializedName("fax")
    @Expose
    private String fax;
    @SerializedName("sales_flag")
    @Expose
    private String salesFlag;
    @SerializedName("employee_type")
    @Expose
    private String employeeType;
    @SerializedName("created_by")
    @Expose
    private Object createdBy;
    @SerializedName("created_date")
    @Expose
    private Object createdDate;
    @SerializedName("last_updated_by")
    @Expose
    private Object lastUpdatedBy;
    @SerializedName("last_updated_date")
    @Expose
    private Object lastUpdatedDate;
    @SerializedName("biometric_empno")
    @Expose
    private String biometricEmpno;
    @SerializedName("relieve_description")
    @Expose
    private Object relieveDescription;
    @SerializedName("notice_period")
    @Expose
    private Object noticePeriod;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("organisation")
    @Expose
    private String organisation;

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getEmployeeNumber() {
        return employeeNumber;
    }

    public void setEmployeeNumber(String employeeNumber) {
        this.employeeNumber = employeeNumber;
    }

    public String getEmployeeCount() {
        return employeeCount;
    }

    public void setEmployeeCount(String employeeCount) {
        this.employeeCount = employeeCount;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getReportingManager() {
        return reportingManager;
    }

    public void setReportingManager(String reportingManager) {
        this.reportingManager = reportingManager;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getFlat() {
        return flat;
    }

    public void setFlat(String flat) {
        this.flat = flat;
    }

    public String getEmploymentStatus() {
        return employmentStatus;
    }

    public void setEmploymentStatus(String employmentStatus) {
        this.employmentStatus = employmentStatus;
    }

    public String getDateOfJoining() {
        return dateOfJoining;
    }

    public void setDateOfJoining(String dateOfJoining) {
        this.dateOfJoining = dateOfJoining;
    }

    public String getDateOfLeaving() {
        return dateOfLeaving;
    }

    public void setDateOfLeaving(String dateOfLeaving) {
        this.dateOfLeaving = dateOfLeaving;
    }

    public String getYearsOfExperience() {
        return yearsOfExperience;
    }

    public void setYearsOfExperience(String yearsOfExperience) {
        this.yearsOfExperience = yearsOfExperience;
    }

    public String getEsiNo() {
        return esiNo;
    }

    public void setEsiNo(String esiNo) {
        this.esiNo = esiNo;
    }

    public String getPfNo() {
        return pfNo;
    }

    public void setPfNo(String pfNo) {
        this.pfNo = pfNo;
    }

    public String getUanNo() {
        return uanNo;
    }

    public void setUanNo(String uanNo) {
        this.uanNo = uanNo;
    }

    public String getWorkTelephoneNumber() {
        return workTelephoneNumber;
    }

    public void setWorkTelephoneNumber(String workTelephoneNumber) {
        this.workTelephoneNumber = workTelephoneNumber;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getSalesFlag() {
        return salesFlag;
    }

    public void setSalesFlag(String salesFlag) {
        this.salesFlag = salesFlag;
    }

    public String getEmployeeType() {
        return employeeType;
    }

    public void setEmployeeType(String employeeType) {
        this.employeeType = employeeType;
    }

    public Object getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Object createdBy) {
        this.createdBy = createdBy;
    }

    public Object getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Object createdDate) {
        this.createdDate = createdDate;
    }

    public Object getLastUpdatedBy() {
        return lastUpdatedBy;
    }

    public void setLastUpdatedBy(Object lastUpdatedBy) {
        this.lastUpdatedBy = lastUpdatedBy;
    }

    public Object getLastUpdatedDate() {
        return lastUpdatedDate;
    }

    public void setLastUpdatedDate(Object lastUpdatedDate) {
        this.lastUpdatedDate = lastUpdatedDate;
    }

    public String getBiometricEmpno() {
        return biometricEmpno;
    }

    public void setBiometricEmpno(String biometricEmpno) {
        this.biometricEmpno = biometricEmpno;
    }

    public Object getRelieveDescription() {
        return relieveDescription;
    }

    public void setRelieveDescription(Object relieveDescription) {
        this.relieveDescription = relieveDescription;
    }

    public Object getNoticePeriod() {
        return noticePeriod;
    }

    public void setNoticePeriod(Object noticePeriod) {
        this.noticePeriod = noticePeriod;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getOrganisation() {
        return organisation;
    }

    public void setOrganisation(String organisation) {
        this.organisation = organisation;
    }
}
