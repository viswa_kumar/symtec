package info.androidhive.navigationdrawer.network;

import android.content.Context;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import info.androidhive.navigationdrawer.R;
import info.androidhive.navigationdrawer.controllerinterface.ResponseListener;
import info.androidhive.navigationdrawer.util.Utility;


/**
 * Created by HOM on 31/08/2016.
 */


public class NetworkUtils {

    // Singleton objects...
    private static NetworkUtils networkUtils = null;


    // Singleton method...
    public static NetworkUtils getInstance() {

        if (networkUtils != null) {
            return networkUtils;
        } else {
            networkUtils = new NetworkUtils();
            return networkUtils;
        }
    }


    public static <T> T getModelFromJson(String json, Class<T> classType) {
        Gson gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.IDENTITY).create();
        return gson.fromJson(json, classType);
    }

    /**
     * Contact API call
     *
     * @param context  Context of current state of the application/object
     * @param listener Interface in which the response is handled
     * @param flag     Represents the API (This is purely developer defined)
     */

   public void contactRequest(Context context, ResponseListener listener, int flag) {
        try {
            if (Utility.getInstances().isDeviceOnLine(context)) {
                String url=context.getResources().getString(R.string.baseurl1);
                RequestHandler.getInstance().getVolley(context,url, listener, flag);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}