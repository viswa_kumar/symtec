package info.androidhive.navigationdrawer.activity;

import android.content.Intent;
import android.content.res.Resources;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

import info.androidhive.navigationdrawer.R;
import info.androidhive.navigationdrawer.fragment.LeaveEntryFragment;
import info.androidhive.navigationdrawer.fragment.Po_Approval_Fragment;
import info.androidhive.navigationdrawer.fragment.HomeFragment;
import info.androidhive.navigationdrawer.fragment.MoviesFragment;
import info.androidhive.navigationdrawer.fragment.SettingsFragment;
import info.androidhive.navigationdrawer.fragment.So_Approval_Fragment;
import info.androidhive.navigationdrawer.fragment.User_Details_Fragment;
import info.androidhive.navigationdrawer.helper.SQLiteHandler;
import info.androidhive.navigationdrawer.helper.SessionManager;
import info.androidhive.navigationdrawer.other.CircleTransform;

public class MainActivity extends AppCompatActivity implements ExpandableListView.OnChildClickListener {

    private NavigationView navigationView;
    private DrawerLayout drawer;
    private View navHeader;
    private ImageView imgNavHeaderBg, imgProfile;
    CircleImageView prof;
    private TextView txtName, orgName, locId, locAddr, compName;
    private Toolbar toolbar;
    private FloatingActionButton fab;
    SessionManager session;
    SQLiteHandler dbase;
    SQLiteDatabase db;
    TextView titles;

    private ExpandableListView drawerList;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;


    private DrawerLayout mDrawerLayout;
    private int mSelectedId;

    private Fragment fragment;
    private FragmentManager fragmentManager;

    // urls to load navigation header background image
    // and profile image
    private static final String urlNavHeaderBg = "http://api.androidhive.info/images/nav-menu-header-bg.jpg";
    private static final String urlProfileImg = "http://192.168.1.230/symtecdevdeploynew/public/uploads/users/51.jpg";

    private static final String myString = "http:\\/\\/192.168.1.230\\/symtecdevdeploynew\\/public\\/uploads\\/users\\/51.jpg";

    String viswa = myString.replaceAll("/", "");

    // index to identify current nav menu item
    public static int navItemIndex = 0;

    // tags used to attach the fragments
    private static final String TAG_HOME = "home";
    private static final String TAG_APPROVAL = "approval";
    private static final String TAG_LEAVEAPPROVAL = "Leave_Entry";
    private static final String TAG_NOTIFICATIONS = "notifications";
    private static final String TAG_SETTINGS = "settings";
    public static String CURRENT_TAG = TAG_HOME;

    // toolbar titles respected to selected nav menu item
    private String[] activityTitles;

    // flag to load home fragment when user presses back key
    private boolean shouldLoadHomeFragOnBackPress = true;
    private Handler mHandler;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setLogo(R.drawable.ifive);
        toolbar.setLogoDescription("viswa");



        mHandler = new Handler();
        Log.w("Image Url", viswa);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        fab = (FloatingActionButton) findViewById(R.id.fab);

        // Navigation view header
        navHeader = navigationView.getHeaderView(0);
        txtName = (TextView) navHeader.findViewById(R.id.name);
        orgName = (TextView) navHeader.findViewById(R.id.org_name);
        locId = (TextView) navHeader.findViewById(R.id.loc_code);
        locAddr = (TextView) navHeader.findViewById(R.id.comp_address);
        compName = (TextView) navHeader.findViewById(R.id.comp_name);
        imgNavHeaderBg = (ImageView) navHeader.findViewById(R.id.img_header_bg);
        prof = (CircleImageView) navHeader.findViewById(R.id.pro_pic);

        dbase = new SQLiteHandler(getApplicationContext());
        // drawerList = (ExpandableListView) findViewById(R.id.left_drawer);

        prepareListData();

/*
        drawerList.setAdapter(new NavAdapter(this, listDataHeader, listDataChild));

        drawerList.setOnChildClickListener(this);
*/

        // load toolbar titles from string resources
        activityTitles = getResources().getStringArray(R.array.nav_item_activity_titles);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        // load nav menu header data
        loadNavHeader();

        // initializing navigation menu
        setUpNavigationView();

        if (savedInstanceState == null) {
            navItemIndex = 0;
            CURRENT_TAG = TAG_HOME;
            loadHomeFragment();
        }


        session = new info.androidhive.navigationdrawer.helper.SessionManager(getApplicationContext());
        if (!session.isLoggedIn()) {
            logoutUser();
        }

    }

    private void prepareListData() {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();


        // Adding headers
        Resources res = getResources();
        String[] headers = res.getStringArray(R.array.nav_drawer_labels);
        listDataHeader = Arrays.asList(headers);


        List<String> home, friends, notifs;
        String[] shome, sfriends, snotifs;

        shome = res.getStringArray(R.array.elements_home);
        home = Arrays.asList(shome);

        listDataChild.put(listDataHeader.get(0), home); // Header, Child data

    }

    /***
     * Load navigation menu header information
     h     * name, website, notifications action view (dot)
     */
    private void loadNavHeader() {

        dbase = new SQLiteHandler(getApplicationContext());

        HashMap<String, String> user = dbase.getUserDetails();
        String name = user.get("name");
        String Trans = user.get("ss_defaultorg_name");
        // String location = user.get("ss_defaultloc_code");
        // name, website
        txtName.setText(name);
        orgName.setText(Trans);

        // loading header background image
      /*  Glide.with(this).load(urlNavHeaderBg)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imgNavHeaderBg);*/

        // Loading profile image
        Glide.with(this).load(urlProfileImg)
                .crossFade()
                .thumbnail(0.5f)
                .bitmapTransform(new CircleTransform(this))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(prof);
        Log.w("viswa dp", viswa);

        // showing dot next to notifications label
        navigationView.getMenu().getItem(3).setActionView(R.layout.menu_dot);
    }

    /***
     * Returns respected fragment that user
     * selected from navigation menu
     */
    private void loadHomeFragment() {
        // selecting appropriate nav menu item
        //    selectNavMenu();

        // set toolbar title
        setToolbarTitle();

        // if user select the current navigation menu again, don't do anything
        // just close the navigation drawer
        if (getSupportFragmentManager().findFragmentByTag(CURRENT_TAG) != null) {
            drawer.closeDrawers();

            // show or hide the fab button
            toggleFab();
            return;
        }

        // Sometimes, when fragment has huge data, screen seems hanging
        // when switching between navigation menus
        // So using runnable, the fragment is loaded with cross fade effect
        // This effect can be seen in GMail app
        Runnable mPendingRunnable = new Runnable() {
            @Override
            public void run() {
                // update the main content by replacing fragments
                Fragment fragment = getHomeFragment();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                        android.R.anim.fade_out);
                fragmentTransaction.replace(R.id.frame, fragment, CURRENT_TAG);
                fragmentTransaction.commitAllowingStateLoss();
            }
        };

        // If mPendingRunnable is not null, then add to the message queue
        if (mPendingRunnable != null) {
            mHandler.post(mPendingRunnable);
        }

        // show or hide the fab button
        toggleFab();

        //Closing drawer on item click
        drawer.closeDrawers();

        // refresh toolbar menu
        invalidateOptionsMenu();
    }

    private Fragment getHomeFragment() {
        switch (navItemIndex) {
            case 0:
                // home
                HomeFragment homeFragment = new HomeFragment();
                return homeFragment;
            case 1:

                User_Details_Fragment user_details_fragment = new User_Details_Fragment();
                return user_details_fragment;

            case 2:
                // notifications fragment
                LeaveEntryFragment leave_approval_fragment = new LeaveEntryFragment();
                return leave_approval_fragment;

            case 3:

                Po_Approval_Fragment apr = new Po_Approval_Fragment();
                return apr;

          /*  case 3:
                // notifications fragment
                Customer_Home notificationsFragment = new Customer_Home();
                return notificationsFragment;*/


            case 4:
                // settings fragment
                SettingsFragment settingsFragment = new SettingsFragment();
                return settingsFragment;
            default:
                return new MoviesFragment();
        }
    }

    private void setToolbarTitle() {
        getSupportActionBar().setTitle(activityTitles[navItemIndex]);
    }


    private void setUpNavigationView() {
        //Setting Navigation View Item Selected Listener to handle the item click of the navigation menu
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            // This method will trigger on item Click of navigation menu
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {

                //Check to see which item was being clicked and perform appropriate action
                switch (menuItem.getItemId()) {
                    //Replacing the main content with ContentFragment Which is our Inbox View;
                    case R.id.nav_home:

                        navItemIndex = 0;
                        // replaceFragment(0);
                        CURRENT_TAG = TAG_HOME;
                        break;
                    //replaceFragment(0);

                    //break;
                    case R.id.nav_photos:
                        navItemIndex = 1;
                        //replaceFragment(0);
                        CURRENT_TAG = TAG_APPROVAL;
                        break;

                    case R.id.nav_leave_approval:
                        navItemIndex = 2;
//                        replaceFragment(0);
                        CURRENT_TAG = TAG_LEAVEAPPROVAL;
                        break;

                    case R.id.nav_notifications:
                        navItemIndex = 3;
                        //replaceFragment(0);
                        CURRENT_TAG = TAG_NOTIFICATIONS;
                        break;


                    default:
                        navItemIndex = 0;
                }


                //Checking if the item is in checked state or not, if not make it in checked state
                if (menuItem.isChecked()) {
                    menuItem.setChecked(false);
                } else {
                    menuItem.setChecked(true);
                }
                menuItem.setChecked(true);

                loadHomeFragment();

                return true;
            }
        });


        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.openDrawer, R.string.closeDrawer) {

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
                super.onDrawerOpened(drawerView);
            }
        };

        //Setting the actionbarToggle to drawer layout
        drawer.setDrawerListener(actionBarDrawerToggle);

        //calling sync state is necessary or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();
    }

    public void replaceFragment(int i) {
        if (i == 0) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (fragment != null) {
                            mDrawerLayout.closeDrawer(GravityCompat.START);
                            fragmentManager.beginTransaction()
                                    .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim
                                            .slide_out_right)
                                    .replace(R.id.frame, fragment)
                                    .commit();
                        }
                    } catch (IllegalStateException ignored) {

                    }
                }
            }, 250);
        }
        mDrawerLayout.closeDrawers();
    }

    private void logoutUser() {
        session.setLogin(true);
        session.setCheckin(true);
        Intent i = new Intent(MainActivity.this, LoginActivity.class);
        startActivity(i);
        dbase.deleteUsers();
        finish();

    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawers();
            return;
        }

        // This code loads home fragment when back key is pressed
        // when user is in other fragment than home
        if (shouldLoadHomeFragOnBackPress) {
            // checking if user is on other navigation menu
            // rather than home
            if (navItemIndex != 0) {
                navItemIndex = 0;
                CURRENT_TAG = TAG_HOME;
                loadHomeFragment();
                return;
            }
        }

        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        // show menu only when home fragment is selected
        if (navItemIndex == 0) {
            getMenuInflater().inflate(R.menu.main, menu);
        }

        // when fragment is notifications, load the menu created for notifications
        if (navItemIndex == 3) {
            getMenuInflater().inflate(R.menu.notifications, menu);
        }
        return true;
    }


    // show or hide the fab
    private void toggleFab() {
        if (navItemIndex == 0)
            fab.show();
        else
            fab.hide();
    }

    @Override
    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {


        String item = listDataChild.get(
                listDataHeader.get(groupPosition)).get(
                childPosition);

        if (item.equals("Po Approval")) {

            Fragment fragment = null;
            fragment = new Po_Approval_Fragment();
            android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frame, fragment);
            fragmentTransaction.addToBackStack(null);
            setTitle("Po Approval");
            fragmentTransaction.commit();

            Toast.makeText(this, "second", Toast.LENGTH_SHORT).show();
            Log.w("check1", "viswan");
            /*Bundle bundle = new Bundle();
            bundle.putString("my_key", "My String");
            Po_Approval_Fragment apr = new Po_Approval_Fragment();
            apr.setArguments(bundle);*/

            Toast.makeText(
                    getApplicationContext(),
                    listDataHeader.get(groupPosition)
                            + " : "
                            + listDataChild.get(
                            listDataHeader.get(groupPosition)).get(
                            childPosition), Toast.LENGTH_SHORT)
                    .show();
        }

        if (item.equals("So Approval")) {

            Fragment fragment = null;
            fragment = new So_Approval_Fragment();
            android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frame, fragment);
            fragmentTransaction.addToBackStack(null);
            setTitle("So Approval");
            fragmentTransaction.commit();

            Toast.makeText(this, "third", Toast.LENGTH_SHORT).show();
            Log.w("check2", "viswan");

            Toast.makeText(
                    getApplicationContext(),
                    listDataHeader.get(groupPosition)
                            + " : "
                            + listDataChild.get(
                            listDataHeader.get(groupPosition)).get(
                            childPosition), Toast.LENGTH_SHORT)
                    .show();
        }


        Toast.makeText(
                getApplicationContext(),
                listDataHeader.get(groupPosition)
                        + " : "
                        + listDataChild.get(
                        listDataHeader.get(groupPosition)).get(
                        childPosition), Toast.LENGTH_SHORT)
                .show();

        return false;
    }
}
