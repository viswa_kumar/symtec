package info.androidhive.navigationdrawer.network;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


/**
 * Created by HOM on 31/08/2016.
 */

public class ResponseHandler {

    // Singleton object...
    private static ResponseHandler responseHandler = null;

    // Singleton method...
    public static ResponseHandler getInstance() {
        if (responseHandler != null) {
            return responseHandler;
        } else {
            responseHandler = new ResponseHandler();
            return responseHandler;
        }
    }

    public static <T> T getModelFromJson(String json, Class<T> classType) {
        Gson gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.IDENTITY).create();
        return gson.fromJson(json, classType);
    }

    /** Parse the json object
     *
     * @param jsonObject which is to be parsed
     * @return json object is parsed and returned as ArrayList<HashMap<String, String>>
     */

    public ArrayList<HashMap<String, String>> parseContactResponse(JSONObject jsonObject) {
        ArrayList<HashMap<String, String>> contactList = new ArrayList<HashMap<String, String>>();

        try {
            JSONObject jsonObj = jsonObject;

            // Getting JSON Array node
            JSONArray result = jsonObj.optJSONArray("result");

            if (result != null && result.length() > 0) {
                // looping through All Contacts

                for (int i = 0; i < result.length(); i++) {
                    JSONObject c = result.optJSONObject(i);

                    /*String id = c.optString("id");
                    String make = c.optString("make");
                    String model = c.optString("model");
                    String year = c.optString("year");
                    String version = c.optString("version");
                    String registration=c.optString("registration_date");
                    String chasis=c.optString("chasis_number");
                    String bike=c.optString("bike_number");
                    String vehicle=c.optString("vehicle_type");
                    String registration1=c.optString("registration");
                    String city=c.optString("city");
                    String state=c.optString("state");
                    String cc=c.optString("cc");
                    String price=c.optString("price_per_day");
                    String description=c.optString("description");
                    String insurance=c.optString("insurance");
                    String rc=c.optString("rc_book");
                    String image=c.optString("image");

                    // Phone node is JSON Object
                    *//*JSONObject phone = c.optJSONObject("phone");
                    String mobile = phone.optString("mobile");
                    String home = phone.optString("home");
                    String office = phone.optString("office");*//*

                    // tmp hashmap for single contact
                    HashMap<String, String> contact = new HashMap<String, String>();
                    // adding each child node to HashMap key => value
                    contact.put("id", id);
                    contact.put("make", make);
                    contact.put("model", model);
                    contact.put("year", year);
                    contact.put("version",version);
                    contact.put("registration_date",registration);
                    contact.put("chasis_number",chasis);
                    contact.put("bike_number",bike);
                    contact.put("vehicle_type",vehicle);
                    contact.put("registration",registration1);
                    contact.put("city",city);
                    contact.put("state",state);
                    contact.put("cc",cc);
                    contact.put("price_per_day",price);
                    contact.put("description",description);
                    contact.put("insurance",insurance);
                    contact.put("rc_book",rc);
                    contact.put("image",image);

                    // adding contact to contact list
                    contactList.add(contact);
*/
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return contactList;
    }

}
