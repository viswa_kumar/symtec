/*
package info.androidhive.navigationdrawer.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import info.androidhive.navigationdrawer.R;

*/
/**
 * Created by Syss4 on 9/13/2017.
 *//*


public class Notification_Home extends Fragment {

    public static Notification_Home instance;

    private NotificationsFragment fragmentOne;
    private OurNotification fragmentTwo;
    private TabLayout allTabs;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.notihome, null);

        instance=this;

        allTabs = (TabLayout)rootView.findViewById(R.id.tabs);
        bindWidgetsWithAnEvent();
        setupTabLayout();
        return rootView;
    }

   */
/* public static Notification_Home getInstance() {
        return instance;
    }*//*




    private void setupTabLayout() {
        fragmentOne = new NotificationsFragment();
        fragmentTwo = new OurNotification();

        allTabs.addTab(allTabs.newTab().setText("CUSTOMER"),true);
        allTabs.addTab(allTabs.newTab().setText("CUSTOMER LOCATION"));
    }

    private void bindWidgetsWithAnEvent()
    {
        allTabs.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                setCurrentTabFragment(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }

    private void setCurrentTabFragment(int tabPosition)
    {
        switch (tabPosition)
        {
            case 0 :
                replaceFragment(fragmentOne);
                break;
            case 1 :
                replaceFragment(fragmentTwo);
                break;
        }
    }

    public void replaceFragment(android.support.v4.app.Fragment fragment) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.frame_container, fragment);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.commit();
    }
}
*/
