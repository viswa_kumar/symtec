/*
package info.androidhive.navigationdrawer;

import android.app.ExpandableListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import info.androidhive.navigationdrawer.activity.MainActivity;
import info.androidhive.navigationdrawer.controllerinterface.ResponseListener;
import info.androidhive.navigationdrawer.helper.SQLiteHandler;
import info.androidhive.navigationdrawer.network.RequestHandler;
import info.androidhive.navigationdrawer.util.Utility;

public class Appro extends AppCompatActivity implements ResponseListener {

	private static final String JSON_URL = "https://simplifiedcoding.net/demos/view-flipper/heroes.php";
	private static final String J_URL = "http://192.168.1.230/symtecdevdeploynew/public/mobileapputlity/poapproval";

	ImageView img;
	int posi;
	Bitmap bmp;
	private ProgressDialog progress;
	public static String picturePath;
	private ListView listView;
	info.androidhive.navigationdrawer.ListAdapter adapter;
	ArrayList<String> dataItems = new ArrayList<String>();

	private int PICK_IMAGE_REQUEST = 1;

	private Bitmap bitmap;
	Uri uridatas;
	ListAdapter listAdapter;
	LinearLayout linearLayout;
	List<Hero> heroList;
	SQLiteHandler dbase;
	TextView org_id, user_id;
	private String TAG = Appro.class.getSimpleName();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.apro);

		listView = (ListView) findViewById(R.id.list_view);
		org_id = (TextView)findViewById(R.id.orgIds);
		user_id = (TextView)findViewById(R.id.userIds);

		heroList = new ArrayList<>();

		dbase = new SQLiteHandler(getApplicationContext());

		HashMap<String, String> user = dbase.getUserDetails();
		String uId = user.get("uid");
		String orgId = user.get("ss_defaultorg_id");

		org_id.setText(orgId);
		user_id.setText(uId);

		loadHeroList();
		getData();

	}

	private void getData() {
		try {
			Log.e("Second", "Welcome");

			Map<String, String> params = new HashMap<String, String>();

			dbase = new SQLiteHandler(getApplicationContext());
			HashMap<String, String> user = dbase.getUserDetails();
			String uId = user.get("uid");
			String orgId = user.get("ss_defaultorg_id");

			org_id.setText(orgId);
			user_id.setText(uId);
			//String userid = SharedPref.getInstance().getStringValue(getActivity(), "patientuser_id");

			params.put("user_id", uId);

			params.put("ss_defaultorg_id", orgId);

			if (Utility.getInstances().isDeviceOnLine(Appro.this)) {
				progress.show();
				String url = "http://192.168.1.230/symtecdevdeploynew/public/mobileapputlity/poapproval";
				RequestHandler.getInstance().aPostFormData(Appro.this, url, params,this,  0);

				Log.e(TAG, "Response from url: " + url);

			} else {
				Toast.makeText(Appro.this, "No Internet", Toast.LENGTH_LONG).show();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	private void loadHeroList() {
		//getting the progressbar
		final ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar);

		//making the progressbar visible
		progressBar.setVisibility(View.VISIBLE);

		//creating a string request to send request to the url
		StringRequest stringRequest = new StringRequest(Request.Method.GET, J_URL,
				new Response.Listener<String>() {
					@Override
					public void onResponse(String response) {
						//hiding the progressbar after completion
						progressBar.setVisibility(View.INVISIBLE);


						try {
							//getting the whole json object from the response
							JSONObject obj = new JSONObject(response);

							//we have the array named hero inside the object
							//so here we are getting that json array
							JSONArray heroArray = obj.getJSONArray("polist");

							//now looping through all the elements of the json array
							for (int i = 0; i < heroArray.length(); i++) {
								//getting the json object of the particular index inside the array
								JSONObject heroObject = heroArray.getJSONObject(i);

								//creating a hero object and giving them the values from json object
								Hero hero = new Hero(heroObject.getString("po_header_id"), heroObject.getString("po_number"));

								//adding the hero to herolist
								heroList.add(hero);
							}

							//creating custom adapter object
							info.androidhive.navigationdrawer.ListAdapter adapter = new info.androidhive.navigationdrawer.ListAdapter(heroList, getApplicationContext());

							//adding the adapter to listview
							listView.setAdapter(adapter);

						} catch (JSONException e) {
							e.printStackTrace();
						}
					}
				},
				new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {

						Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
					}
				});


		RequestQueue requestQueue = Volley.newRequestQueue(this);

		requestQueue.add(stringRequest);
	}

	@Override
	public void successResponse(String successResponse, int flag) {


	}

	@Override
	public void successResponse(JSONObject jsonObject, int flag) {

	}

	@Override
	public void errorResponse(String errorResponse, int flag) {

	}

	@Override
	public void removeProgress(Boolean hideFlag) {

	}
}*/
