package info.androidhive.navigationdrawer.util;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.widget.Toast;

/**
 * Created by HOM on 31/08/2016.
 */

public class AlertContext {

    //Single ton object...
    private static AlertContext alertContext = null;

    //Single ton method...
    public static AlertContext getInstance() {
        if (alertContext != null) {
            return alertContext;
        } else {
            alertContext = new AlertContext();
            return alertContext;
        }
    }

    /**
     * Network error dialog.
     *
     * @param context Context of current state of the application/object
     */

   /* public void networkDialog(Context context) {

        String title = context.getResources().getString(R.string.networkError);
        String message = context.getResources().getString(R.string.checkYourInternetConnection);
        String buttonText = context.getResources().getString(R.string.oK);

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(buttonText, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }
*/
    /**
     * Dialog which is used to show the common message with "Ok" button
     * .i.e., with only positive button.
     *
     * @param context Context of current state of the application/object.
     * @param title   String value which will be displayed as a title for the alert dialog.
     * @param message String value which will be displayed as a message for the alert dialog.
     */

    public void alertDialog(Context context, String title, String message) {
        String buttonText = "Ok";
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(buttonText, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }


    /**
     * Dialog which contains both positive and negative button.
     * If there is any navigation to the activity for the positive button click listener, can be done.
     *
     * @param context        Context of current state of the application/object.
     * @param title          String value which will be displayed as a title for the alert dialog.
     * @param message        String value which will be displayed as a message for the alert dialog.
     * @param positiveBtName Positive button name
     * @param negativeBtName Negative button name
     * @param intent         Object of the intent if avail.
     */


    public void alertDialog(final Context context, String title, String message, String positiveBtName, String negativeBtName,
                            final Intent intent) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(positiveBtName, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                /**
                 * Checking whether the intent object is not gender1,
                 * if not then the navigation to that activity is done...
                 */
                if (intent != null) {
                    context.startActivity(intent);
                }
            }
        });
        builder.setNegativeButton(negativeBtName, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }


    /**
     * Display a toast message.
     *
     * @param context  Context of current state of the application/object
     * @param messages Text which has to be displayed in toast
     */

    public void showToastMessages(Context context, String messages) {
        Toast.makeText(context, messages, Toast.LENGTH_SHORT).show();
    }


}
