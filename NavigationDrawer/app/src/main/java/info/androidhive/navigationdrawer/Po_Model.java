package info.androidhive.navigationdrawer;

/**
 * Created by Syss4 on 9/15/2017.
 */

public class Po_Model {
    String pono, pohead, podate, empid, buyername, username, supplierid, suppliername, status, remark, orgid, supervisorid, user_id;

    public Po_Model(String pono, String pohead, String podate, String empid, String buyername, String username,
                    String supplierid, String suppliername, String status, String remark, String orgid, String supervisorid, String user_id) {
        this.pono = pono;
        this.pohead = pohead;
        this.podate = podate;
        this.empid = empid;
        this.buyername = buyername;
        this.username = username;
        this.supplierid = supplierid;
        this.suppliername = suppliername;
        this.status = status;
        this.remark = remark;
        this.orgid = orgid;
        this.supervisorid = supervisorid;
        this.user_id = user_id;
    }

    public String getPono() {
        return pono;
    }

    public String getPohead() {
        return pohead;
    }

    public String getPodate() {
        return podate;
    }

    public String getEmpid() {
        return empid;
    }

    public String getBuyername() {
        return buyername;
    }

    public String getUsername() {
        return username;
    }

    public String getSupplierid() {
        return supplierid;
    }

    public String getSuppliername() {
        return suppliername;
    }

    public String getStatus() {
        return status;
    }

    public String getRemark() {
        return remark;
    }

    public String getOrgid() {
        return orgid;
    }

    public String getSupervisorid() {
        return supervisorid;
    }

    public String getUser_id() {
        return supervisorid;
    }
}