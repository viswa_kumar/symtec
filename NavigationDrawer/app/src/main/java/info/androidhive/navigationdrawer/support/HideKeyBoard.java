package info.androidhive.navigationdrawer.support;

import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

/**
 * Created by Rocky on 9/26/2015.
 */
public class HideKeyBoard {
    Context context;
    InputMethodManager inputManager = null;

    public HideKeyBoard(Context context) {
        this.context = context;
    }

    public void hideSoftKeyboard(View v) {
        if (context != null) {
            inputManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        }
        inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }
}
