package info.androidhive.navigationdrawer.activity;

import android.os.Bundle;
import android.widget.TextView;

import info.androidhive.navigationdrawer.Approval_Activity;
import info.androidhive.navigationdrawer.R;

/**
 * Created by Syss4 on 9/18/2017.
 */

public class Po_Approval_View extends Approval_Activity {

    TextView poNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.po_approval_view);
        poNumber = (TextView) findViewById(R.id.poNo);
        String poN = getIntent().getStringExtra("poNumber");
        poNumber.setText(poN);
    }
}
