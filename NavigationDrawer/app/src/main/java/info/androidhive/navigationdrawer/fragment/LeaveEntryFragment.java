package info.androidhive.navigationdrawer.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.Unbinder;
import info.androidhive.navigationdrawer.Noti_Model;
import info.androidhive.navigationdrawer.R;
import info.androidhive.navigationdrawer.adapters.spinner_adapters.LeaveTypeSpinnerAdapter;
import info.androidhive.navigationdrawer.adapters.spinner_adapters.ReportManagerSpinnerAdapter;
import info.androidhive.navigationdrawer.adapters.spinner_adapters.TypeLeaveSpinnerAdapter;
import info.androidhive.navigationdrawer.datas.models.requests.LeaveFormRequests;
import info.androidhive.navigationdrawer.datas.models.responses.LeaveFormResponse;
import info.androidhive.navigationdrawer.datas.remote.UserAPICall;
import info.androidhive.navigationdrawer.engine.RetroFitEngine;
import info.androidhive.navigationdrawer.engine.HRMSEngine;
import info.androidhive.navigationdrawer.helper.SQLiteHandler;
import info.androidhive.navigationdrawer.helper.SessionManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Syss4 on 12/28/2017.
 */

public class LeaveEntryFragment extends Fragment {

    SessionManager sessionManager;
    private SQLiteHandler db;

    SQLiteHandler dbase;
    Context appContext;
    TextView org_id, user_id;
    List<Noti_Model> notify_ModelList;
    private ListView listView;
    private Spinner sp;
    private ArrayList<String> students;
    SwipeRefreshLayout refresh;
    @BindView(R.id.default_manager)
    TextView defaultManager;
    @BindView(R.id.change_manager)
    Spinner changeManager;
    @BindView(R.id.leave_type)
    Spinner leaveTypeSpinner;
    @BindView(R.id.type_leave_spinner)
    Spinner typeLeaveSpinner;
    LinearLayout lay1,lay2, lay3, lay4;


    View view;
    String uId;
    private ProgressDialog pDialog;
    LeaveFormResponse leaveFormResponse;
    LeaveFormRequests leaveFormRequests;
    private Unbinder unbinder;
    RecyclerView.LayoutManager mLayoutManager;
    Context myContext;
    LeaveTypeSpinnerAdapter leaveTypeSpinnerAdapter;
    TypeLeaveSpinnerAdapter typeLeaveSpinnerAdapter;
    ReportManagerSpinnerAdapter reportManagerSpinnerAdapter;

    @Override
    public void onAttach(Activity activity) {
        myContext=(FragmentActivity) activity;
        getActivity().setTitle("Collection Details");
        super.onAttach(activity);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.leave_entry, null);
        listView = rootView.findViewById(R.id.list_view);
        refresh = rootView.findViewById(R.id.swipe_company);
        students = new ArrayList<String>();
        sp = rootView.findViewById(R.id.spinner);
        sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                // TODO Auto-generated method stub
                Toast.makeText(getActivity(), sp.getSelectedItem().toString(),
                        Toast.LENGTH_SHORT).show();
            }
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });
        notify_ModelList = new ArrayList<>();
        dbase = new SQLiteHandler(getActivity());
        HashMap<String, String> user = dbase.getUserDetails();
        uId = user.get("uid");
        String orgId = user.get("ss_defaultorg_id");
//        org_id.setText(orgId);
       // user_id.setText(uId);
        lay1 = rootView.findViewById(R.id.type);
        lay2 = rootView.findViewById(R.id.model);
        lay3 = rootView.findViewById(R.id.hours);
        lay4 = rootView.findViewById(R.id.days);
        pDialog = new ProgressDialog(getActivity());
        pDialog.setCancelable(false);
        sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String item = parent.getItemAtPosition(position).toString();
                Toast.makeText(parent.getContext(), "Selected: " + item, Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
//        String uid = user_id.getText().toString();
       // String org_ids = org_id.getText().toString();
       // checkLogin(uid, org_ids);
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    public void getCollectionDetails() {
        pDialog.show();
        leaveFormRequests = new LeaveFormRequests();
        leaveFormRequests.setEmployeeId(HRMSEngine.myInstance.convertStringToInt(uId));
        leaveFormRequests.setLeaveType(true);
        leaveFormRequests.setReportingManager(true);
        leaveFormRequests.setTypeLeave(true);
        leaveFormResponse = new LeaveFormResponse();
        UserAPICall userAPICall = RetroFitEngine.getRetrofit().create(UserAPICall.class);
        Call<LeaveFormResponse> callEnqueue = userAPICall.leaveRequest(leaveFormRequests);
        callEnqueue.enqueue(new Callback<LeaveFormResponse>() {
            @Override
            public void onResponse(Call<LeaveFormResponse> call, Response<LeaveFormResponse> response) {
                leaveFormResponse = response.body();
                if(leaveFormResponse != null){
                    setLeaveTypeSpinner();
                    setTypeLeaveSpinner();
                    defaultManager.setText(leaveFormResponse.getEmployeeDetails().getDefaultManager().getFirstName() + " "
                            +leaveFormResponse.getEmployeeDetails().getDefaultManager().getLastName());
                    setReportManagerSpinner();
                }
                pDialog.dismiss();
            }
            @Override
            public void onFailure(Call<LeaveFormResponse> call, Throwable t) {
                pDialog.dismiss();
                Toast.makeText(myContext,  t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setReportManagerSpinner() {
        reportManagerSpinnerAdapter= new ReportManagerSpinnerAdapter(myContext,
                android.R.layout.simple_spinner_item,
                leaveFormResponse.getEmployeeDetails().getAllManager());
        changeManager.setAdapter(reportManagerSpinnerAdapter);
        changeManager.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int position, long id) {
                // Here you get the current item (a User object) that is selected by its position
                HRMSEngine.myInstance.reportManagerSelected = reportManagerSpinnerAdapter.getItem(position);
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapter) {  }
        });
    }

    private void setTypeLeaveSpinner() {
        typeLeaveSpinnerAdapter= new TypeLeaveSpinnerAdapter(myContext,
                android.R.layout.simple_spinner_item,
                leaveFormResponse.getTypeLeave());
        typeLeaveSpinner.setAdapter(typeLeaveSpinnerAdapter);
        typeLeaveSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int position, long id) {
                // Here you get the current item (a User object) that is selected by its position
                HRMSEngine.myInstance.typeLeaveSelected = typeLeaveSpinnerAdapter.getItem(position);
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapter) {  }
        });
    }

    private void setLeaveTypeSpinner() {
        leaveTypeSpinnerAdapter= new LeaveTypeSpinnerAdapter(myContext,
                android.R.layout.simple_spinner_item,
                leaveFormResponse.getLeaveType());
        leaveTypeSpinner.setAdapter(leaveTypeSpinnerAdapter);
        leaveTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int position, long id) {
                // Here you get the current item (a User object) that is selected by its position
                HRMSEngine.myInstance.leaveTypeSelected = leaveTypeSpinnerAdapter.getItem(position);
                if (leaveTypeSpinnerAdapter.getItem(position).getLookupCode().equals("CASUAL LEAVE")) {
                    lay2.setVisibility(View.VISIBLE);
                    lay3.setVisibility(View.GONE);
                    lay4.setVisibility(View.VISIBLE);
                }else if (leaveTypeSpinnerAdapter.getItem(position).getLookupCode().equals("SICK LEAVE")) {
                    lay2.setVisibility(View.VISIBLE);
                    lay3.setVisibility(View.GONE);
                    lay4.setVisibility(View.VISIBLE);
                }else if (leaveTypeSpinnerAdapter.getItem(position).getLookupCode().equals("ON DUTY")) {
                    //lay3.setVisibility(View.VISIBLE);
                    lay2.setVisibility(View.GONE);
                    lay3.setVisibility(View.VISIBLE);
                    lay4.setVisibility(View.GONE);
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapter) {  }
        });
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
