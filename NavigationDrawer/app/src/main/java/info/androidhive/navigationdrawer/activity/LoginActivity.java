package info.androidhive.navigationdrawer.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import info.androidhive.navigationdrawer.R;
import info.androidhive.navigationdrawer.app.AppConfig;
import info.androidhive.navigationdrawer.app.AppController;
import info.androidhive.navigationdrawer.expend.Main;
import info.androidhive.navigationdrawer.helper.SQLiteHandler;
import info.androidhive.navigationdrawer.helper.SessionManager;
import info.androidhive.navigationdrawer.support.HideKeyBoard;

import static info.androidhive.navigationdrawer.app.AppController.TAG;

/**
 * Created by Syss4 on 9/12/2017.
 */

public class LoginActivity extends Activity {

    EditText eMail, passWord;
    TextView acc, com, ids;
    Button login;
    TextView not_reg, imei;
    SessionManager sessionManager;
    private SQLiteHandler db;
    String uName, uPwd;
    private HideKeyBoard hideKeyBoard;

    private ProgressDialog pDialog;
    private SessionManager session;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        initViews();

        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        CheckBox c = (CheckBox) findViewById(R.id.show);

        c.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!isChecked) {
                    passWord.setTransformationMethod(PasswordTransformationMethod.getInstance());
                } else {
                    passWord.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }
            }
        });


        // Session manager
        session = new SessionManager(getApplicationContext());

        hideKeyBoard = new HideKeyBoard(LoginActivity.this);

        if (session.isLoggedIn()) {
            // User is already logged in. Take him to menu_main activity
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
        }

        /*findViewById(R.id.linearLayout4).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                hideKeyBoard.hideSoftKeyboard(v);
                return false;
            }
        });*/


        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        db = new SQLiteHandler(getApplicationContext());

        session = new SessionManager(getApplicationContext());

        if (session.isLoggedIn()) {
            // User is already logged in. Take him to menu_main activity
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
        }

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (eMail.getText().toString().length() == 0) {
                    eMail.setError("Email id is required!");
                }


                if (passWord.getText().toString().length() == 0) {
                    passWord.setError("Wrong Password!");
                } else {
                    String email = eMail.getText().toString().trim();
                    String password = passWord.getText().toString().trim();
                    String access = acc.getText().toString().trim();


                    Log.d(email, "This is my message");
                    // Check for empty data in the form
                    if (!email.isEmpty() && !password.isEmpty()) {
                        // login user
                        checkLogin(email, password, access);


                    } else {
                        // Prompt user to enter credentials
                        Toast.makeText(LoginActivity.this, "Please enter correct email & password", Toast.LENGTH_SHORT).show();

                    }
                }
            }
        });
    }

    private void initViews() {
        eMail = (EditText) findViewById(R.id.username);

        passWord = (EditText) findViewById(R.id.password);

        login = (Button) findViewById(R.id.login);
        //not_reg = (TextView) findViewById(R.id.not_reg);
        acc = (TextView) findViewById(R.id.comIds);

        com = (TextView) findViewById(R.id.company_name);

        //ids = (TextView)findViewById(R.id.comIds)


        String cmp = getIntent().getStringExtra("comp_name");
        String ids = getIntent().getStringExtra("company_id");
        com.setText(cmp);
        acc.setText(ids);
    }

    private void checkLogin(final String email, final String password, final String access) {
        // Tag used to cancel the request
        String tag_string_req = "req_login";

        pDialog.setMessage("Logging in ...");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST, AppConfig.URL_LOGIN, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Login Response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {
                        // user successfully logged in
                        // Create login session
                        session.setLogin(true);

                        // Now store the user in SQLite
                        String uid = jObj.getString("user_id");

                        Log.d("Login uid", uid);

                        JSONObject user = jObj.getJSONObject("userdetails");
                        String name = user.getString("username");
                        String email = user.getString("email");
                        String ss_defaultorg_name = user.getString("ss_defaultorg_name");
                        String ss_defaultloc_code = user.getString("ss_defaultloc_code");
                        String ss_defaultloc_name = user.getString("ss_defaultloc_name");
                        String ss_defaultorg_id = user.getString("ss_defaultorg_id");
                        String first_name = user.getString("first_name");


                        Log.e("first_name", first_name);
                        // Inserting row in users table
                        db.addUser(name, email, uid, ss_defaultorg_name, ss_defaultloc_code, ss_defaultloc_name, ss_defaultorg_id, first_name);

                        // Launch menu_main activity
                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                    } else {
                        // Error in login. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Login Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("username", email);
                params.put("password", password);
                params.put("company", access);
                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

   /* @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }*/

    public final boolean isInternetOn() {

        // get Connectivity Manager object to check connection
        ConnectivityManager connec =
                (ConnectivityManager) getSystemService(getBaseContext().CONNECTIVITY_SERVICE);

        // Check for network connections
        if (connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED) {

            // if connected with internet

//            Toast.makeText(this, " Connected ", Toast.LENGTH_SHORT).show();
            return true;

        } else if (
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED ||
                        connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED) {

            Toast.makeText(this, "Please Check Your Internet Connection!", Toast.LENGTH_SHORT).show();
            return false;
        }
        return false;
    }
}
