package info.androidhive.navigationdrawer.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;
import info.androidhive.navigationdrawer.R;
import info.androidhive.navigationdrawer.helper.SQLiteHandler;

/**
 * Created by Syss4 on 9/15/2017.
 */

public class User_Details extends AppCompatActivity {

    TextView fName, emId, org_name, com_name, lCode, lAddr, depa, last_login;
    CircleImageView proPic;
    SQLiteHandler dbase;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_details);

        fName = (TextView)findViewById(R.id.fName);
        emId = (TextView)findViewById(R.id.eId);
        proPic = (CircleImageView)findViewById(R.id.pro_pics);
        org_name = (TextView)findViewById(R.id.orgName);
        com_name = (TextView)findViewById(R.id.comName);
        lCode = (TextView)findViewById(R.id.locCode);
        lAddr = (TextView)findViewById(R.id.locName);
        depa = (TextView)findViewById(R.id.dep);
        last_login = (TextView)findViewById(R.id.lastLog);

        dbase = new SQLiteHandler(getApplicationContext());

        HashMap<String, String> user = dbase.getUserDetails();
        String first_name = user.get("first_name");
        String Trans = user.get("email");
        String org = user.get("ss_defaultorg_name");
        //String cName = user.get("")
        String lcodes = user.get("ss_defaultloc_code");
        String lAddress = user.get("ss_defaultloc_name");
        //String depas = user.get()

        fName.setText(first_name);
        emId.setText(Trans);
        org_name.setText(org);
        lCode.setText(lcodes);
        lAddr.setText(lAddress);
    }
}
