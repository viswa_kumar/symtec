package info.androidhive.navigationdrawer.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.media.Image;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import info.androidhive.navigationdrawer.R;
import info.androidhive.navigationdrawer.app.AppConfig;
import info.androidhive.navigationdrawer.fragment.NotificationsFragment;
import info.androidhive.navigationdrawer.support.HttpHandler;

import static info.androidhive.navigationdrawer.R.id.app;
import static info.androidhive.navigationdrawer.app.AppConfig.URL_CUSTOMER;

/**
 * Created by Syss4 on 9/13/2017.
 */

public class Company extends AppCompatActivity {

    ImageView sym, sep;
    ListView cList, cId;
    private ProgressDialog pDialog;
    private static String url = "http://192.168.1.230/symtecdevdeploynew/public/poutlity/companydetails";
    //private static String url = "http://ifive.sytes.net/symtecapp/symtecdevdeploynew/public/poutlity/companydetails";
    ArrayList<HashMap<String, String>> contactList;
    SwipeRefreshLayout refresh;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.compeny);


        cList = (ListView) findViewById(R.id.company_list);
        refresh = (SwipeRefreshLayout)findViewById(R.id.swipe_company);

        refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshContent();
            }
        });

        contactList = new ArrayList<>();
        new GetContacts().execute();

        cList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                TextView cName = (TextView) view.findViewById(R.id.companyName);
                TextView cid = (TextView) view.findViewById(R.id.comId);


                String comp_name = cName.getText().toString();
                String comp_id = cid.getText().toString();


                Intent modify_intent = new Intent(getApplicationContext(), LoginActivity.class);

                modify_intent.putExtra("comp_name", comp_name);
                modify_intent.putExtra("company_id", comp_id);

                startActivity(modify_intent);


            }
        });


    }

    private void refreshContent(){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                contactList = new ArrayList<>();
                new GetContacts().execute();
                refresh.setRefreshing(false);
            }
        }, 3000);
    }

    private class GetContacts extends AsyncTask<Void, Void, Void> {

        private String TAG = MainActivity.class.getSimpleName();

        @Override
        protected Void doInBackground(Void... params) {

            HttpHandler sh = new HttpHandler();

            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall(AppConfig.COMPANY);

            Log.e(TAG, "Response from url: " + jsonStr);

            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);

                    // Getting JSON Array node
                    JSONArray contacts = jsonObj.getJSONArray("company_details");

                    // looping through All Contacts
                    for (int i = 0; i < contacts.length(); i++) {
                        JSONObject c = contacts.getJSONObject(i);

                        /*HashMap<String, String> user = dbase.getUserDetails();
                        String uid = user.get("uid");*/

                            String company_id = c.getString("company_id");
                        String company_code = c.getString("company_code");
                        String name = c.getString("name");

                        Log.w("Comapny_name", name);
                        HashMap<String, String> customers = new HashMap<>();

                        // adding each child node to HashMap key => value
                        customers.put("name", name);
                        customers.put("company_id", company_id);


                        // adding contact to contact list
                        contactList.add(customers);
                    }
                } catch (final JSONException e) {
                    Log.e(TAG, "Json parsing error: " + e.getMessage());
                    Company.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(Company.this,
                                    "Json parsing error: " + e.getMessage(),
                                    Toast.LENGTH_LONG)
                                    .show();
                        }
                    });

                }
            } else {
                Log.e(TAG, "Couldn't get json from server.");
                Company.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(Company.this,
                                "Couldn't get json from server. Check LogCat for possible errors!",
                                Toast.LENGTH_LONG)
                                .show();
                    }
                });

            }

            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(Company.this);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();
            /**
             * Updating parsed JSON data into ListView
             * */
            ListAdapter adapter = new SimpleAdapter(
                    Company.this, contactList,
                    R.layout.company_list_tem, new String[]{"name", "company_id"}, new int[]{R.id.companyName, R.id.comId});


            cList.setAdapter(adapter);
        }

    }
}
