package info.androidhive.navigationdrawer.controllerinterface;

import org.json.JSONObject;

/**
 * Created by HOM on 30/08/2016.
 */

public interface ResponseListener {

    public void successResponse(String successResponse, int flag);

    public void successResponse(JSONObject jsonObject, int flag);

    public void errorResponse(String errorResponse, int flag);

    public void removeProgress(Boolean hideFlag);
}
