package info.androidhive.navigationdrawer;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import info.androidhive.navigationdrawer.app.AppConfig;
import info.androidhive.navigationdrawer.app.AppController;
import info.androidhive.navigationdrawer.helper.SQLiteHandler;

import static info.androidhive.navigationdrawer.app.AppController.TAG;

/**
 * Created by Syss4 on 9/21/2017.
 */

public class So_List_Adapter extends ArrayAdapter<Po_Model> {

    So_List_Adapter.customButtonListener customListner;
    private int PICK_IMAGE_REQUEST = 1;
    public static int pos;
    private List<Po_Model> poModelList;
    ArrayList<String> datas = new ArrayList<String>();
    Context appContext;
    private ProgressDialog pDialog;
    private SQLiteHandler db;

    public interface customButtonListener {
        public void onButtonClickListner(int position, String value);
    }

    public void setCustomButtonListner(So_List_Adapter.customButtonListener listener) {
        this.customListner = listener;
    }

    private Context context = null;
    private ArrayList<String> data = new ArrayList<String>();

    public So_List_Adapter(List<Po_Model> poModelList, Context context) {
        super(context, R.layout.so_list_item, poModelList);
        this.poModelList = poModelList;
        this.context = context;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final So_List_Adapter.ViewHolder viewHolder;
        pos = position;
        if (convertView == null) {

            final LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.so_list_item, null);
            viewHolder = new So_List_Adapter.ViewHolder();
            viewHolder.text = (TextView) convertView
                    .findViewById(R.id.soNo);

            viewHolder.podate = (TextView) convertView
                    .findViewById(R.id.soDate);

            viewHolder.supplier = (TextView) convertView
                    .findViewById(R.id.soSupplier);

            viewHolder.head = (TextView) convertView.findViewById(R.id.notific_id);
            viewHolder.u_id = (TextView) convertView.findViewById(R.id.sos_id);

            viewHolder.poActions = (Button) convertView.findViewById(R.id.so_id_action);
            viewHolder.poView = (Button) convertView.findViewById(R.id.so_id_view);


            //  window.setLayout(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);


            viewHolder.poActions.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(context, "Approval", Toast.LENGTH_SHORT).show();
                    // dialog.show();

                    Log.e("viswa_context", "" + appContext);

                    final Dialog dialog = new Dialog(getContext(), android.R.style.Theme_DeviceDefault_Light_Dialog);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.so_popup_window);
                    TextView poNumber = (TextView) dialog.findViewById(R.id.popup_soNumber);
                    final TextView poHeader = (TextView)dialog.findViewById(R.id.so_header);
                    final TextView poUid = (TextView)dialog.findViewById(R.id.so_uid);
                    final EditText remark = (EditText) dialog.findViewById(R.id.so_comments);
                    Button approval = (Button) dialog.findViewById(R.id.so_approval);
                    Button cancel = (Button) dialog.findViewById(R.id.so_cancel);
                    //ImageView display_img = (ImageView) dialog.findViewById(R.id.bgvfdhj);
                    poNumber.setText(viewHolder.text.getText().toString());
                    poUid.setText(viewHolder.u_id.getText().toString());
                    poHeader.setText(viewHolder.head.getText().toString());
                    dialog.show();

                    approval.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            String status = "APPROVED";
                            String remarks = remark.getText().toString();
                            String po_header_id = poHeader.getText().toString();
                            String user_id = poUid.getText().toString();
                            checkLogin(status, remarks, po_header_id, user_id);
                            dialog.dismiss();
                            Toast.makeText(context, "Po Approved successfully", Toast.LENGTH_SHORT).show();
                        }
                    });

                    cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            String status = "REJECTED";
                            String remarks = remark.getText().toString();
                            String po_header_id = poHeader.getText().toString();
                            String user_id = poUid.getText().toString();
                            checkLogin(status, remarks, po_header_id, user_id);
                            dialog.dismiss();
                            Toast.makeText(context, "Po Approved successfully", Toast.LENGTH_SHORT).show();

                            dialog.dismiss();
                        }
                    });


                }
            });

            viewHolder.poView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(context, "View" + viewHolder.text.getText().toString(), Toast.LENGTH_SHORT).show();

                  /*  Intent c = new Intent(context, Po_Approval_View.class);
                    c.putExtra("poNumber", viewHolder.text.getText().toString());
                    c.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(c);*/

                }
            });

           /* viewHolder.podate = (TextView) convertView.findViewById(R.id.poDate);
            viewHolder.amount = (TextView) convertView.findViewById(R.id.poamount);
            viewHolder.supplier = (TextView) convertView.findViewById(R.id.poSupplier);*/

            viewHolder.linearLayout = (LinearLayout) convertView.findViewById(R.id.sohiding);
            viewHolder.linearLayout1 = (LinearLayout) convertView.findViewById(R.id.sovisi);
            convertView.setTag(viewHolder);

            Po_Model poModel = poModelList.get(position);

            viewHolder.text.setText(poModel.getPono());
            viewHolder.podate.setText(poModel.getPodate());
            viewHolder.supplier.setText(poModel.getSuppliername());
            viewHolder.head.setText(poModel.getPohead());

            db = new SQLiteHandler(getContext());

            HashMap<String, String> user = db.getUserDetails();
            String uId = user.get("uid");

            viewHolder.u_id.setText(uId);

            Log.w("texteeee", poModel.getPodate());
            Log.e("texteeee1", poModel.getSuppliername());

        } else {
            viewHolder = (So_List_Adapter.ViewHolder) convertView.getTag();
        }


        viewHolder.linearLayout1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  viewHolder.linearLayout.setVisibility(View.VISIBLE);
                //viewHolder.linearLayout.setVisibility(View.INVISIBLE);
                if (viewHolder.linearLayout.getVisibility() == View.VISIBLE) {
                    viewHolder.linearLayout.setVisibility(View.INVISIBLE);

                }
                viewHolder.linearLayout.setVisibility(View.VISIBLE);
            }
        });

      /*  viewHolder.button.setOnClickListener(new View.OnClickListener() {
            //            String imgs = datas.get(position);
            @Override
            public void onClick(View v) {
                if (customListner != null) {

                }

            }
        });*/


        return convertView;
    }

    private void checkLogin(final String status, final String remarks, final String po_header_id, final String user_id) {
        // Tag used to cancel the request
        String tag_string_req = "req_login";


          /*  pDialog.setMessage("Logging in ...");
            showDialog();
*/

        StringRequest strReq = new StringRequest(Request.Method.POST, AppConfig.PO_APPCONFIRM, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Approval Response: " + response.toString());
                //hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                    Log.w("Aruna", "Viswa");
                    // Check for error node in json
                    if (!error) {
                        // user successfully logged in
                        // Create login session

                        Log.w("Viswa", "kumar");
                        // Now store the user in SQLite

                        /*JSONArray polist = jObj.getJSONArray("polist");
                        for (int i = 0; i < polist.length(); i++) {

                            Log.w("sai", "pavan");

                            JSONObject c = polist.getJSONObject(i);

                            String po_number = c.getString("po_number");
                            String po_header_id = c.getString("po_header_id");
                            String po_date = c.getString("po_date");
                            String emp_id = c.getString("emp_id");
                            String buyername = c.getString("buyername");
                            String username = c.getString("username");
                            String supplier_id = c.getString("supplier_id");
                            String supplier_name = c.getString("supplier_name");
                            String po_approval_status = c.getString("po_approval_status");
                            String remarks = c.getString("remarks");
                            String organization_id = c.getString("organization_id");
                            String supervisor_user_id = c.getString("supervisor_user_id");

                            Log.w("poNumber", po_number);
                            Log.e("podate", po_date);
                            Log.d("empId", emp_id);
                            Po_Model hero = new Po_Model(po_number, po_header_id, po_date, emp_id, buyername, username, supplier_id, supplier_name, po_approval_status, remarks, organization_id, supervisor_user_id);
                            poModelList.add(hero);

//finish();
                        }*/


                    } else {
                        // Error in login. Get the error message
                        String errorMsg = jObj.getString("message");
                        Toast.makeText(context,
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(context, "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Login Error: " + error.getMessage());
                Toast.makeText(context,
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                /*String status = "APPROVED";
                String remarks = "viswannn";
                String po_header_id = "7";
                String user_id = "15";*/

               /* String orgid = org_id.getText().toString();
                String uid = user_id.getText().toString();*/

                params.put("status", status);
                params.put("remarks", remarks);
                params.put("po_header_id", po_header_id);
                params.put("user_id", user_id);
                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void showDialog() {

        if (!pDialog.isShowing())
            pDialog.show();

    }

    private void hideDialog() {

        if (pDialog.isShowing())
            pDialog.dismiss();

    }

    public class ViewHolder {
        TextView text, podate, amount, supplier, head, u_id;
        EditText remark;
        Button poActions, poView;
        ImageView imgs;
        LinearLayout linearLayout;
        LinearLayout linearLayout1;
    }
}
