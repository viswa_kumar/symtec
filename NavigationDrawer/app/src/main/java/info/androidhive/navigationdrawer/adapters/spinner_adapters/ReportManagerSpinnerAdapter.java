package info.androidhive.navigationdrawer.adapters.spinner_adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import info.androidhive.navigationdrawer.datas.models.responses.AllManager;
import info.androidhive.navigationdrawer.datas.models.responses.LeaveType;

/**
 * Created by Comp11 on 1/9/2018.
 */

public class ReportManagerSpinnerAdapter  extends ArrayAdapter<AllManager> {

    // Your sent context
    private Context context;
    // Your custom values for the spinner (User)
    private List<AllManager> values;

    public ReportManagerSpinnerAdapter(Context context, int textViewResourceId,
                                   List<AllManager> values) {
        super(context, textViewResourceId, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public int getCount(){
        return values.size();
    }

    @Override
    public AllManager getItem(int position){
        return values.get(position);
    }

    @Override
    public long getItemId(int position){
        return position;
    }

    // And the "magic" goes here
    // This is for the "passive" state of the spinner
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // I created a dynamic TextView here, but you can reference your own  custom layout for each spinner item
        TextView label = new TextView(context);
        label.setTextColor(Color.BLACK);
        label.setTextSize(15);
        label.setPadding(10,10,10,10);
        // Then you can get the current item using the values array (Users array) and the current position
        // You can NOW reference each method you has created in your bean object (User class)
        label.setText(values.get(position).getFirstName()+" "+values.get(position).getLastName() );
        // And finally return your dynamic (or custom) view for each spinner item
        return label;
    }

    // And here is when the "chooser" is popped up
    // Normally is the same view, but you can customize it if you want
    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        TextView label = new TextView(context);
        label.setTextColor(Color.BLACK);
        label.setTextSize(15);
        label.setText(values.get(position).getFirstName()+" "+values.get(position).getLastName() );
        label.setPadding(10,10,10,10);
        return label;
    }
}
