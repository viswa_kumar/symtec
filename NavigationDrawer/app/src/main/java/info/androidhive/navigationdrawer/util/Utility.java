package info.androidhive.navigationdrawer.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by HOM on 31/08/2016.
 */

public class Utility {


    //Single ton object...
    private static Utility utility = null;

    //Single ton method...
    public static Utility getInstances() {
        if (utility != null) {
            return utility;
        } else {
            utility = new Utility();
            return utility;
        }
    }



    /** Checks whether the device currently has a network connection..
     *
     * @param context Context of current state of the application/object
     * @return true if the device has a network connection, false otherwise.
     */

    public boolean isDeviceOnLine(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo info = connectivity.getActiveNetworkInfo();
            if (info != null) {
                if (info.getState() == NetworkInfo.State.CONNECTED) {
                    return true;
                }
            }
        }
        return false;
    }


}
