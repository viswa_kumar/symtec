package info.androidhive.navigationdrawer.datas.remote;

import java.util.List;

import info.androidhive.navigationdrawer.datas.models.requests.LeaveFormRequests;
import info.androidhive.navigationdrawer.datas.models.responses.LeaveFormResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface UserAPICall {

    @POST("symtecapp/symtecdevdeployc/public/api-list-datas")
    Call<LeaveFormResponse> leaveRequest(@Body LeaveFormRequests leaveFormRequests);

}
