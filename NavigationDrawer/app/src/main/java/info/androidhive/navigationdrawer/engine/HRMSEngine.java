package info.androidhive.navigationdrawer.engine;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.Snackbar;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import info.androidhive.navigationdrawer.datas.models.responses.AllManager;
import info.androidhive.navigationdrawer.datas.models.responses.LeaveType;
import info.androidhive.navigationdrawer.datas.models.responses.TypeLeave;

/**
 * Created by Comp11 on 12/19/2017.
 */

public class HRMSEngine {

    public static ProgressDialog progressDialog;
    public static HRMSEngine myInstance = new HRMSEngine();
    public Integer collectionID;
    public Integer shippingID;// strings at index 0 is not used, it is to make array
    // indexing simple
    String one[] = { "", "one ", "two ", "three ", "four ",
            "five ", "six ", "seven ", "eight ",
            "nine ", "ten ", "eleven ", "twelve ",
            "thirteen ", "fourteen ", "fifteen ",
            "sixteen ", "seventeen ", "eighteen ",
            "nineteen "
    };

    // strings at index 0 and 1 are not used, they is to
// make array indexing simple
    String ten[] = { "", "", "twenty ", "thirty ", "forty ",
            "fifty ", "sixty ", "seventy ", "eighty ",
            "ninety "
    };
    public LeaveType leaveTypeSelected;
    public TypeLeave typeLeaveSelected;
    public AllManager reportManagerSelected;

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
    }

    public boolean isEmpty(String string) {
        if (string.trim().equals("")) {
            return true;
        } else
            return false;
    }

    public double convertStringToDouble(String text){
        double number=0;
        try {
            number = Double.parseDouble(text);
        } catch(NumberFormatException e) {
        }
        return number;
    }

    public int convertDoubleToInt(double text){
        int number=0;
        try {
            number = (int) Math.floor(text);
        } catch(NumberFormatException e) {
        }
        return number;
    }

    public int convertStringToInt(String text){
        int number=0;
        try {
            number = Integer.parseInt(text);
        } catch(NumberFormatException e) {
        }
        return number;
    }

    public void snackbarNoInternet(Context context) {
        Snackbar snackbar = Snackbar
                .make(((Activity)context).findViewById(android.R.id.content),
                        "No Network Connection!", Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    public static ProgressDialog getProgDialog(Context context) {
        ProgressDialog progDialog = new ProgressDialog(context);
        progDialog.setMessage("Loading...");
        progDialog.setIndeterminate(false);
        progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progDialog.setCancelable(false);
        return progDialog;
    }

    public static String getCurrentDate(){
        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }

    public static String getCurrentTime(){
        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());
        SimpleDateFormat df = new SimpleDateFormat("hh:mm:ss aa");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }

    public double getGST(double totalAmount, double gstSelected) {
        try{
            return (totalAmount*gstSelected)/100;
        }catch(Exception e){

        }
        return 0;
    }

    public Animation getBlinkAnimation(){
        Animation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(1000); //You can manage the blinking time with this parameter
        anim.setStartOffset(20);
        anim.setRepeatMode(Animation.REVERSE);
        anim.setRepeatCount(Animation.INFINITE);
        return anim;
    }

    public String getCalenderTime(Calendar myCalendar) {
        String date = myCalendar
                .get(Calendar.YEAR)+"-"+myCalendar
                .get(Calendar.MONTH)+"-"+myCalendar
                .get(Calendar.DAY_OF_MONTH)+" "+myCalendar
                .get(Calendar.HOUR_OF_DAY)+":"+myCalendar
                .get(Calendar.MINUTE)+":"+myCalendar
                .get(Calendar.SECOND);
        return date;
    }

    public String formatDate(String reqDate) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy");
        try {
             return sdf1.format(sdf.parse(reqDate));
        } catch (ParseException e) {
            e.printStackTrace();
            System.out.println("bad pattern");
        }
        return "";
    }

    public String formatDateTime(String reqDate) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        try {
            return sdf1.format(sdf.parse(reqDate));
        } catch (ParseException e) {
            e.printStackTrace();
            System.out.println("bad pattern");
        }
        return "";
    }

    public String convertToWords(double nUm){
        int n = (convertDoubleToInt(nUm));
        String out ="";
        if((n%10000000)>=1){
            out += numToWords((n / 10000000), "crore ");
        }
        if((n%100000)>=1){
            out += numToWords((n / 100000), "lakh ");
        }
        if((n%1000)>=1){
            out += numToWords((n / 1000), "thousand ");
        }
        if((n%100)>=1){
            out += numToWords((n / 100), "hundred ");
        }
        if ((n >100)&&(n%100!=0)){
            out += "and ";
            out += numToWords((n / 100), "");
        }
        return out;
    }

    public String  numToWords(int n, String s){
        String str = "";
        // if n is more than 19, divide it
        if (n > 19)
            str += ten[convertDoubleToInt(n/10)] + one[convertDoubleToInt(n%10)];
        else
            str += one[convertDoubleToInt(n)];
        // if n is non-zero
        if (n!=0)
            str += s;
        return str;
    }
}
