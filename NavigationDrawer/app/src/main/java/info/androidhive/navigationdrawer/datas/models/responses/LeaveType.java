package info.androidhive.navigationdrawer.datas.models.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Comp11 on 1/9/2018.
 */

public class LeaveType {
    @SerializedName("lookupvalue_id")
    @Expose
    private String lookupvalueId;
    @SerializedName("lookuptype_id")
    @Expose
    private String lookuptypeId;
    @SerializedName("lookup_code")
    @Expose
    private String lookupCode;
    @SerializedName("lookup_meaning")
    @Expose
    private String lookupMeaning;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("default_code")
    @Expose
    private String defaultCode;
    @SerializedName("additional_refvalue")
    @Expose
    private String additionalRefvalue;
    @SerializedName("sequence")
    @Expose
    private String sequence;
    @SerializedName("active")
    @Expose
    private String active;
    @SerializedName("start_date")
    @Expose
    private String startDate;
    @SerializedName("end_date")
    @Expose
    private String endDate;
    @SerializedName("organization_id")
    @Expose
    private String organizationId;
    @SerializedName("created_by")
    @Expose
    private Object createdBy;
    @SerializedName("created_date")
    @Expose
    private Object createdDate;
    @SerializedName("last_updated_by")
    @Expose
    private Object lastUpdatedBy;
    @SerializedName("last_updated_date")
    @Expose
    private Object lastUpdatedDate;

    public String getLookupvalueId() {
        return lookupvalueId;
    }

    public void setLookupvalueId(String lookupvalueId) {
        this.lookupvalueId = lookupvalueId;
    }

    public String getLookuptypeId() {
        return lookuptypeId;
    }

    public void setLookuptypeId(String lookuptypeId) {
        this.lookuptypeId = lookuptypeId;
    }

    public String getLookupCode() {
        return lookupCode;
    }

    public void setLookupCode(String lookupCode) {
        this.lookupCode = lookupCode;
    }

    public String getLookupMeaning() {
        return lookupMeaning;
    }

    public void setLookupMeaning(String lookupMeaning) {
        this.lookupMeaning = lookupMeaning;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDefaultCode() {
        return defaultCode;
    }

    public void setDefaultCode(String defaultCode) {
        this.defaultCode = defaultCode;
    }

    public String getAdditionalRefvalue() {
        return additionalRefvalue;
    }

    public void setAdditionalRefvalue(String additionalRefvalue) {
        this.additionalRefvalue = additionalRefvalue;
    }

    public String getSequence() {
        return sequence;
    }

    public void setSequence(String sequence) {
        this.sequence = sequence;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }

    public Object getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Object createdBy) {
        this.createdBy = createdBy;
    }

    public Object getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Object createdDate) {
        this.createdDate = createdDate;
    }

    public Object getLastUpdatedBy() {
        return lastUpdatedBy;
    }

    public void setLastUpdatedBy(Object lastUpdatedBy) {
        this.lastUpdatedBy = lastUpdatedBy;
    }

    public Object getLastUpdatedDate() {
        return lastUpdatedDate;
    }

    public void setLastUpdatedDate(Object lastUpdatedDate) {
        this.lastUpdatedDate = lastUpdatedDate;
    }
}
