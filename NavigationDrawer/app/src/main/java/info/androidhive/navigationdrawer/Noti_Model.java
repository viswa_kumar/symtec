package info.androidhive.navigationdrawer;

/**
 * Created by Syss4 on 9/25/2017.
 */

public class Noti_Model {
    String userName, date, message, header;

    public Noti_Model(String userName,String date, String message, String header) {
        this.userName = userName;
        this.date = date;
        this.message = message;
        this.header = header;

    }

    public String getUserName() {
        return userName;
    }

    public String getDate() {
        return date;
    }

    public String getMessage() {
        return message;
    }

    public String getHeader() {
        return header;
    }

}
