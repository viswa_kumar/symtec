package info.androidhive.navigationdrawer.network;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

import info.androidhive.navigationdrawer.R;
import info.androidhive.navigationdrawer.controllerinterface.ResponseListener;
import info.androidhive.navigationdrawer.util.AlertContext;

/**
 * Created by HOM on 31/08/2016.
 */


public class RequestHandler {


    //Single ton object...
    private static RequestHandler requestHandler = null;

    //Single ton method...
    public static RequestHandler getInstance() {
        if (requestHandler != null) {
            return requestHandler;
        } else {
            requestHandler = new RequestHandler();
            return requestHandler;
        }
    }

    public void postWithHeader(final Context context, String url, JSONObject jsonObject, final ResponseListener listener, final int flag) {


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject jsonObject) {

                /** Success Response
                 *
                 */

                listener.successResponse(jsonObject, flag);
            }


        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                /** Error Response
                 *
                 */

                listener.removeProgress(true);

                NetworkResponse networkResponse = error.networkResponse;
                String local = error.getLocalizedMessage();
                if (!TextUtils.isEmpty(local)) {
                    /** Error Message **/
                } else if (networkResponse != null) {

                    if (networkResponse.statusCode != 200) {
                        byte[] arr = networkResponse.data;
                        AlertContext.getInstance().alertDialog(context,"Response Error", exceptionMessage(arr));
                    }
                }

            }
        }) {

            /** Setting the content type
             *
             */

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            /** Setting the custom header.
             *
             */

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                return null;
            }

        };

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        VolleySingleton.getInstance(context).addToRequestQueue(jsonObjReq);

    }

    /**
     * Post Request handling method without header.
     *
     * @param context    Context of current state of the application/object
     * @param url        API
     * @param jsonObject Request to be send
     * @param listener   Interface in which the response is handled
     * @param flag       Represents the API (This is purely developer defined)
     */


    public void postWithoutHeader(final Context context, String url, JSONObject jsonObject, final ResponseListener listener, final int flag) {


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject responseJson) {

                VolleyLog.d("successd", responseJson.toString());

                System.out.println("jsonObject.toString()::" + responseJson.toString());


                /** Success Response
                 *
                 */

                listener.successResponse(responseJson, flag);
            }


        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                VolleyLog.d("error", error.getMessage());

                /** Error Response
                 *
                 */

                listener.removeProgress(true);


                NetworkResponse networkResponse = error.networkResponse;
                String local = error.getLocalizedMessage();
                if (!TextUtils.isEmpty(local)) {
                    /** Error Message **/
                } else if (networkResponse != null) {

                    if (networkResponse.statusCode != 200) {
                        byte[] arr = networkResponse.data;
                        AlertContext.getInstance().alertDialog(context, "Error Response", exceptionMessage(arr));
                    }
                }

            }
        }) {

            /** Setting the content type
             *
             */

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

        };


        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        VolleySingleton.getInstance(context).addToRequestQueue(jsonObjReq);


    }


    /**
     * GET Request handling method, Return the response in string.
     *
     * @param context  Context of current state of the application/object
     * @param url      API
     * @param listener Interface in which the response is handled
     * @param flag     Represents the API (This is purely developer defined)
     */

    public void postVolley(final Context context, String url, JSONObject jsonObject, final
    ResponseListener listener, final int flag) {

        System.out.println("POST url::" + url);
        System.out.println("jsonObject.toString()::" + jsonObject.toString());

        //url = generateUrl(context, url);
        System.out.println("POST url::" + url);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject responseJson) {


                VolleyLog.d("successd", responseJson.toString());

                System.out.println("jsonObject.toString()::" + responseJson.toString());


                /** Success Response
                 *
                 */
                if (listener != null) {
                    listener.successResponse(responseJson, flag);

                }
            }


        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                VolleyLog.d("error", error.getMessage());

                /** Error Response
                 *
                 */

                if (listener != null) {
                    listener.removeProgress(true);
                    listener.errorResponse(error.getMessage(), flag);
                }
                NetworkResponse networkResponse = error.networkResponse;


                if (networkResponse != null) {

                    if (networkResponse.statusCode != 200) {


                        VolleyLog.d("Network Error", "Error");
                    }
                }


            }
        }) {


            /** Setting the content type
             *
             */

            @Override
            public String getBodyContentType() {
                return "application/json;charset=UTF-8";
            }

        };


        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        VolleySingleton.getInstance(context).addToRequestQueue(jsonObjReq);


    }

    public void getVolley(final Context context, final String url, final ResponseListener listener, final int flag) {


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject jsonObject) {

                /** Success Response
                 *
                 */

                listener.successResponse(jsonObject, flag);

            }


        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                /** Error Response
                 *
                 */

                listener.removeProgress(true);

                NetworkResponse networkResponse = error.networkResponse;
                String local = error.getLocalizedMessage();
                if (!TextUtils.isEmpty(local)) {
                    /** Error Message **/
                } else if (networkResponse != null) {

                    if (networkResponse.statusCode != 200) {
                        byte[] arr = networkResponse.data;
                        AlertContext.getInstance().alertDialog(context, "Response Error", exceptionMessage(arr));
                    }
                }

            }
        }) {

            /** Setting the content type
             *
             */

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            /** Setting the custom header.
             *
             */

        };

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        VolleySingleton.getInstance(context).addToRequestQueue(jsonObjReq);

    }

    /**
     * GET Request handling method without header, Return the response in string.
     *
     * @param context  Context of current state of the application/object
     * @param url      API
     * @param listener Interface in which the response is handled
     * @param flag     Represents the API (This is purely developer defined)
     */

    public void getWithoutHeader(final Context context, String url, final
    ResponseListener listener, final int flag) {

        System.out.println("url::" + url);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject jsonObject) {

                System.out.println("111 jsonObject::" + jsonObject.toString());

                /** Success Response
                 *
                 */

                listener.successResponse(jsonObject, flag);

            }

        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                /** Error Response
                 *
                 */

                listener.removeProgress(true);

                NetworkResponse networkResponse = error.networkResponse;
                String local = error.getLocalizedMessage();
                if (!TextUtils.isEmpty(local)) {
                    /** Error Message **/
                } else if (networkResponse != null) {

                    if (networkResponse.statusCode != 200) {
                        byte[] arr = networkResponse.data;
                        AlertContext.getInstance().alertDialog(context, "Error Response", exceptionMessage(arr));
                    }
                }

            }
        }) {

            /**
             * Setting the content type
             */

            @Override
            public String getBodyContentType() {
                return "application/json";
            }
        };

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        VolleySingleton.getInstance(context).addToRequestQueue(jsonObjReq);

    }

    /**
     * DELETE Request handling method, Return the response in string.
     *
     * @param context  Context of current state of the application/object
     * @param url      API
     * @param listener Interface in which the response is handled
     * @param flag     Represents the API (This is purely developer defined)
     */

    public void deleteVolley(final Context context, String url, JSONObject jsonObject, final ResponseListener listener, final int flag) {


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.DELETE, url, jsonObject, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject jsonObject) {

                /** Success Response
                 *
                 */

                listener.successResponse(jsonObject, flag);

            }

        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                /** Error Response
                 *
                 */

                listener.removeProgress(true);

                NetworkResponse networkResponse = error.networkResponse;
                String local = error.getLocalizedMessage();
                if (!TextUtils.isEmpty(local)) {
                    /** Error Message **/
                } else if (networkResponse != null) {

                    if (networkResponse.statusCode != 200) {
                        byte[] arr = networkResponse.data;
                        AlertContext.getInstance().alertDialog(context, "Error Response", exceptionMessage(arr));
                    }
                }

            }

        }) {

            /** Setting the content type
             *
             */

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            /** Setting the custom header, in some cases header is not needed.
             *   So header flag is maintained.
             *
             */

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                return null;

            }

        };

        VolleySingleton.getInstance(context).addToRequestQueue(jsonObjReq);

    }


    public  void aPostFormData(final Context context, String url, final Map<String,String> params, final ResponseListener responseListener, final int flag)
    {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Toast.makeText(context,response,Toast.LENGTH_LONG).show();
                        Log.e("onResponse",response);
                        Log.e("Eight","Welcome");
                        responseListener.successResponse(response,flag);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("onResponse",""+error);
                        responseListener.errorResponse("",flag);

                        //Toast.makeText(context,error.toString(),Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String,String> getParams(){

                return params;

            }

        };
        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);

    }

        /**
     * Converts the byte[] message to json object and send the exceptional message
     *
     * @param message error message in byte [] format.
     * @return error message in string format.
     */


    private String exceptionMessage(byte[] message) {
        String strMessage = new String(message);
        try {
            JSONObject jsonObject = new JSONObject(strMessage);
            if (jsonObject != null) {
                return jsonObject.optString("exceptionMessage");
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }


}
