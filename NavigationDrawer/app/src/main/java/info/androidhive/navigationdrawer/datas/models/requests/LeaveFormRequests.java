package info.androidhive.navigationdrawer.datas.models.requests;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Comp11 on 1/9/2018.
 */

public class LeaveFormRequests {

    @SerializedName("leave_type")
    @Expose
    private Boolean leaveType;
    @SerializedName("reporting_manager")
    @Expose
    private Boolean reportingManager;
    @SerializedName("employee_id")
    @Expose
    private Integer employeeId;
    @SerializedName("type_leave")
    @Expose
    private Boolean typeLeave;

    public Boolean getLeaveType() {
        return leaveType;
    }

    public void setLeaveType(Boolean leaveType) {
        this.leaveType = leaveType;
    }

    public Boolean getReportingManager() {
        return reportingManager;
    }

    public void setReportingManager(Boolean reportingManager) {
        this.reportingManager = reportingManager;
    }

    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    public Boolean getTypeLeave() {
        return typeLeave;
    }

    public void setTypeLeave(Boolean typeLeave) {
        this.typeLeave = typeLeave;
    }
}
