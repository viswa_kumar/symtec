package info.androidhive.navigationdrawer.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import info.androidhive.navigationdrawer.R;
import info.androidhive.navigationdrawer.fragment.Notification_Me;
import info.androidhive.navigationdrawer.fragment.NotificationsFragment;
import info.androidhive.navigationdrawer.fragment.Po_Approval_Fragment;

/**
 * Created by Syss4 on 2/24/2017.
 */

public class Customer_Home extends Fragment {

    public static Customer_Home instance;

    private NotificationsFragment fragmentOne;
    private Notification_Me fragmentTwo;
    private TabLayout allTabs;
    Context appContext;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.child_item, null);

        allTabs = (TabLayout)rootView.findViewById(R.id.tabs);

        bindWidgetsWithAnEvent();
        setupTabLayout();

        return rootView;
    }

    /*@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.child_item);
        instance=this;

        getAllWidgets();
        bindWidgetsWithAnEvent();
        setupTabLayout();
    }*/

    public static Customer_Home getInstance() {
        return instance;
    }

    /*private void getAllWidgets() {
        allTabs = (TabLayout)findViewById(R.id.tabs);
    }*/

    private void setupTabLayout() {
        fragmentOne = new NotificationsFragment();
        fragmentTwo = new Notification_Me();

        allTabs.addTab(allTabs.newTab().setText("Notification"),true);
        allTabs.addTab(allTabs.newTab().setText("Notification From Me"));
    }

    private void bindWidgetsWithAnEvent()
    {
        allTabs.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                setCurrentTabFragment(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }

    private void setCurrentTabFragment(int tabPosition)
    {
        switch (tabPosition)
        {
            case 0 :
                replaceFragment(fragmentOne);
                break;
            case 1 :
                replaceFragment(fragmentTwo);
                break;
        }
    }

    public void replaceFragment(Fragment fragment) {
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.frame_container, fragment);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.commit();
    }
}