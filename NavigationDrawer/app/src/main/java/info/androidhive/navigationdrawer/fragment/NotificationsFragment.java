package info.androidhive.navigationdrawer.fragment;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import info.androidhive.navigationdrawer.Noti_Model;
import info.androidhive.navigationdrawer.Notify_ListAdapter;
import info.androidhive.navigationdrawer.Po_Model;
import info.androidhive.navigationdrawer.R;
import info.androidhive.navigationdrawer.activity.Company;
import info.androidhive.navigationdrawer.activity.MainActivity;
import info.androidhive.navigationdrawer.activity.NotificationReceiverActivity;
import info.androidhive.navigationdrawer.app.AppConfig;
import info.androidhive.navigationdrawer.app.AppController;
import info.androidhive.navigationdrawer.helper.SQLiteHandler;
import info.androidhive.navigationdrawer.helper.SessionManager;
import info.androidhive.navigationdrawer.support.HideKeyBoard;
import info.androidhive.navigationdrawer.support.HttpHandler;

import static android.content.Context.NOTIFICATION_SERVICE;
import static info.androidhive.navigationdrawer.app.AppConfig.URL_CUSTOMER;
import static info.androidhive.navigationdrawer.app.AppController.TAG;


public class NotificationsFragment extends Fragment {


    SQLiteHandler dbase;

    TextView org_id, user_id;
    List<Noti_Model> notify_ModelList;
    private ListView listView;

    private ProgressDialog pDialog;

    private ArrayList<String> students;
    SwipeRefreshLayout refresh;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.apro, null);

        listView = (ListView)rootView.findViewById(R.id.list_view);
        org_id = (TextView)rootView.findViewById(R.id.orgIds);
        user_id = (TextView)rootView.findViewById(R.id.userIds);
        //  logs = (Button)rootView.findViewById(R.id.log);

        refresh = (SwipeRefreshLayout)rootView.findViewById(R.id.swipe_company);

        refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshContent();
            }
        });

        students = new ArrayList<String>();

        //Initializing Spinner
        Spinner spinner = (Spinner)rootView.findViewById(R.id.spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),
                R.array.symptoms, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);


        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItem = parent.getItemAtPosition(position).toString();
                switch (selectedItem) {
                    case "Select one Item":
                        break;
                    case "Common Fever":
                        Toast.makeText(getContext(), selectedItem, Toast.LENGTH_SHORT).show();
                        break;
                    case "Dark Circles":
                        Toast.makeText(getContext(), selectedItem, Toast.LENGTH_SHORT).show();
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        notify_ModelList = new ArrayList<>();

        dbase = new SQLiteHandler(getActivity());

        HashMap<String, String> user = dbase.getUserDetails();
        String uId = user.get("uid");
        String orgId = user.get("ss_defaultorg_id");

        org_id.setText(orgId);
        user_id.setText(uId);


        pDialog = new ProgressDialog(getActivity());
        pDialog.setCancelable(false);


        String uid = user_id.getText().toString();
        String org_ids = org_id.getText().toString();

        checkLogin(uid, org_ids);

        return rootView;
    }

    private void refreshContent(){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

               // contactList = new ArrayList<>();
                String uid = user_id.getText().toString();
                String org_ids = org_id.getText().toString();

                checkLogin(uid, org_ids);
                //adapter.clear();
                refresh.setRefreshing(false);
            }
        }, 3000);
    }


    private void checkLogin(final String uid, final String orgid) {
        // Tag used to cancel the request
        String tag_string_req = "req_login";

        pDialog.setMessage("Loading ...");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST, AppConfig.NOTIFICATION, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Approval Response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                    Log.w("Aruna", "Viswa");
                    // Check for error node in json
                    if (!error) {
                        // user successfully logged in
                        // Create login session

                        Log.w("Viswa", "kumar");
                        // Now store the user in SQLite

                        JSONArray polist = jObj.getJSONArray("list");
                        for (int i = 0; i < polist.length(); i++) {

                            Log.w("sai", "pavan");

                            JSONObject c = polist.getJSONObject(i);

                            String po_number = c.getString("to_user");
                            String po_header_id = c.getString("start_date");
                            String po_date = c.getString("message_name");
                            String noti_header = c.getString("wf_notification_id");

                            students.add(c.getString("message_name"));

                            Log.w("heaDER", noti_header);
                           /* String emp_id = c.getString("from_user");
                            String buyername = c.getString("buyername");
                            String username = c.getString("username");
                            String supplier_id = c.getString("supplier_id");
                            String supplier_name = c.getString("supplier_name");
                            String po_approval_status = c.getString("po_approval_status");
                            String remarks = c.getString("remarks");
                            String organization_id = c.getString("organization_id");
                            String supervisor_user_id = c.getString("supervisor_user_id");
                            String user_id = c.getString("user_id");

                            Log.w("poNumber", po_number);
                            Log.e("podate", po_date);
                            Log.d("empId", emp_id);
                            Log.w("uId" ,user_id);*/
                            Noti_Model notify = new Noti_Model(po_number, po_header_id, po_date, noti_header);
                            notify_ModelList.add(notify);

//finish();
                        }

                        info.androidhive.navigationdrawer.Notify_ListAdapter adapter = new info.androidhive.navigationdrawer.Notify_ListAdapter(notify_ModelList, getActivity());
//                        spinner.setAdapter(new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_dropdown_item, students));
                        listView.setAdapter(adapter);

                    } else {
                        // Error in login. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getActivity(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Login Error: " + error.getMessage());
                Toast.makeText(getActivity(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
               /* String orgid = "17";
                String uid = "51";*/


                String uid = user_id.getText().toString();


                params.put("user_id", uid);
                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }


}